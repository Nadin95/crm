<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminLoginController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\FrontCategoryController;
use App\Http\Controllers\FrontProductController;
use App\Http\Controllers\StockManageController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[HomeController::class,'showhome']);
Route::get('/all_category',[FrontCategoryController::class,'showcategoeies']);


Route::get('/item/display/{id}',[FrontProductController::class,'showproduct']);
// Route::get('/item',[FrontProductController::class,'showproduct']);





Route::get('admin/login',function(){
    return view('login/adminlogin');
});
Route::get('/Signout',[AdminLoginController::class,'Signout']);

Route::get('/AdminLogin',[AdminLoginController::class,'showLogin']);
Route::post('/check_admin',[AdminLoginController::class,'check_admin']);




Route::middleware(['adminlogin'])->group(function () {

    Route::prefix('admin/')->group(function()
    {

        Route::get('admin',[AdminController::class,'ViewAdminAdd']);
        Route::post('CreateAdmin',[AdminController::class,'CreateAdmin']);
        Route::post('getEditAdmin',[AdminController::class,'EditAdmin']);
        Route::post('DeleteAdmin/{id}',[AdminController::class,'DeleteAdmin']);
        // Route::get('adminlist',[AdminController::class,'ViewAdminList']);
        Route::get('index',[AdminController::class,'ViewAdmin'])->name('dashboard');


        Route::prefix('category/')->group(function()
        {

            Route::get('/',[CategoryController::class,'ViewCategories']);
            Route::get('create',[CategoryController::class,'ViewAddCategory']);
            Route::post('Savecategory',[CategoryController::class,'AddCategory']);

            Route::get('view/{id}',[CategoryController::class,'ViewCategory']);
            Route::get('update_view/{id}',[CategoryController::class,'ViewCategoryUpdate']);
            Route::post('updatecategory',[CategoryController::class,'CategoryUpdate']);
            Route::post('deletecategory',[CategoryController::class,'CategoryDelete']);



                Route::prefix('subcategory/')->group(function()
                    {
                        Route::get('view/{id}',[CategoryController::class,'ViewSubCategoey']);
                        Route::post('savesubcategory',[CategoryController::class,'AddSubCategory']);
                        Route::get('update_view/{id}',[CategoryController::class,'ViewSubCategoryUpdate']);
                        Route::post('updatecategory',[CategoryController::class,'SubCategoryUpdate']);
                        Route::post('delete',[CategoryController::class,'SubCategoryDelete']);

                            Route::prefix('subsubcategory/')->group(function()
                            {
                                Route::get('view/{id}',[CategoryController::class,'view_subsubCategory']);
                                Route::post('save_subsub_category',[CategoryController::class,'Add_SubSub_Category']);
                                Route::get('update_view/{id}',[CategoryController::class,'View_Edit_SubSub_Category']);
                                Route::post('updatecategory',[CategoryController::class,'Edit_subsub_Category']);
                                Route::post('delete/',[CategoryController::class,'Delete_SubSub_Category']);
                            });



                        // Route::get([CategoryController::class,'View_Edit_SubSub_Category']);

                     });







        });


     Route::prefix('product/')->group(function ()
     {

        Route::get('addview',[ProductController::class,'createproductview']);
        Route::get('addviewvariantform',[ProductController::class,'viewvariant']);
        Route::post('upload_product',[ProductController::class,'createproduct']);
        Route::get('allproducts',[ProductController::class,'viewproductlist']);
        Route::get('editproduct/{id}',[ProductController::class,'editview']);
        Route::post('update_product',[ProductController::class,'updateproduct']);
        Route::get('editproduct/variant_edit/{id}',[ProductController::class,'vieweditvariant']);
        Route::post('update_product_variants',[ProductController::class,'updatevariant']);
        Route::post('remove_image/{id}',[ProductController::class,'removeimage']);


     });


        Route::prefix('grn/')->group(function ()
        {
            Route::get('addview',[StockManageController::class,'creategrnview']);
            Route::post('creategrn',[StockManageController::class,'creategrn']);
            Route::get('edit/{id}',[StockManageController::class,'editgrnview']);
            Route::post('update',[StockManageController::class,'updategrnVatiants']);
            Route::get('view',[StockManageController::class,'viewgrn']);
            Route::get('updategrn/{id}',[StockManageController::class,'viewupdategrn']);






        });



    });

});
