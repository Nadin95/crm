@extends('layout.admin_layout')
@section('content')


    <form method="POST" action="{{ url('admin/product/update_product') }}" enctype="multipart/form-data" id="image-upload"
        class="uploader">
        @csrf

        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Update Product</h6>

            </div>

            <div class="card-body ">




                <div class="card-body ">

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @php

                    @endphp



                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Categoeies</label>
                        <div class="col-md-10">
                            <select class="form-control select-search" aria-label=".form-select-lg example" name="cat_id"
                                value="{{ $product->name }}" id="cat_id" required>
                                <option selected disabled>Open this select menu</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" @if ($category->id == $product->category_id) selected @endif>{{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Sub Categoey</label>
                        <div class="col-md-10">

                            <select class="form-control select-search" aria-label=".form-select-lg example"
                                name="sub_cat_id" id="sub_cat_id" required>
                                <option selected disabled>Open this select menu</option>
                                @foreach ($subcategories as $category)
                                    <option value="{{ $category->id }}" @if ($category->id == $product->subcategory_id) selected @endif>{{ $category->name }}
                                    </option>

                                @endforeach
                            </select>

                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Sub Sub Category</label>
                        <div class="col-md-10">
                            <select class="form-control  select-search" aria-label=".form-select-lg example"
                                name="subsub_cat_id" id="subsub_cat_id" required>
                                <option selected disabled>Open this select menu</option>

                                @foreach ($sub_sub_categories as $category)
                                    <option value="{{ $category->id }}" @if ($category->id == $product->sub_subcategory_id) selected @endif>{{ $category->name }}
                                    </option>

                                @endforeach
                            </select>
                        </div>
                    </div>




                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Title</label>
                        <div class="col-md-10">
                            <input class="form-control" id="title" name="title" type="text"
                                value="{{ $product->title }}" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Product code </label>
                        <div class="col-md-10">
                            <input class="form-control" id="p_code" name="p_code" type="text"
                                value="{{ $product->product_code }}" required>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Name</label>
                        <div class="col-md-10">
                            <input class="form-control" id="name" name="name" type="text" value="{{ $product->name }}"
                                required>

                        </div>
                    </div>



                    <div class="form-group row">

                        <label class="col-form-label col-md-2" class="col-form-label col-lg-2">Description</label>
                        <div class="col-lg-10">
                            <textarea rows="3" cols="3" class="form-control" id="des" name="des"
                                placeholder="{{ $product->description }}" required></textarea>
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="image-display">
                            @foreach ($product_image as $item)

                                <a href="" onclick="admin/product.delete_Image({{ $item->id }})">X</a><br>
                                <img src="{{ $item->image }}" alt="" height="100px" width="100px">

                            @endforeach

                        </div>
                    </div>





                    <div class="form-group row">
                        <label class=" ol-form-label col-lg-2">Upload Images</label>
                        <div class="fileupload btn btn-secondary col-lg-10">

                            <input type="file" id="files" name="files[]" class="form-control-uniform"
                                data-max-file-size="3M" multiple="multiple" accept="image/*" onchange="" />
                        </div>

                    </div>






                </div>

            </div>
        </div>
        @if ($product_variants->isNotEmpty())
            <div class="card">

                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Edit Varient</h6>

                </div>

                <div class="card-body ">


                    <table class="table table-bordered" id="dynamicAddRemove">
                        <tr>


                            <th>variant</th>
                            <th>View Variant</th>
                        </tr>



                        @foreach ($product_variants as $variant)
                            <tr>

                                <td>
                                    <div class="col-md-4">
                                        <label for="">Width</label>
                                        <input class="form-control" type="text" value="{{ $variant->width }}"
                                            disabled />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Height</label>
                                        <input type="text" value="{{ $variant->height }}" class="form-control"
                                            disabled />
                                    </div>
                                    <div class="col-md-4">

                                </td>

                                <td><a href="variant_edit/{{ $variant->id }}" type="button" name="add" id="dynamic-ar"
                                        class="btn btn-outline-primary">viewVariant</a>

                                </td>
                            </tr>
                        @endforeach
                    </table>

                </div>

            </div>

        @endif







        <div class="card">

            @if (!empty($product))
                <input type="hidden" name="product_id" value="{{ $product->product_id }}">
            @else
                <input type="hidden" name="product_id" value="1">
            @endif

            <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                Update Product
            </button>

        </div>


    </form>


@endsection
