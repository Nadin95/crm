<tr>

    <td>
        <div class="row">
            <div class="col-md-4">
                <label for="">Width</label>
                <input class="form-control" name="variant_width[]" placeholder="Enter width" type="text" required />
            </div>
            <div class="col-md-4">
                <label for="">Height</label>
                <input type="text" name="variant_height[]" placeholder="Enter Height" class="form-control" />
            </div>
            <div class="col-md-4">
                <label for="">Length</label>
                <input type="text" name="variant_length[]" placeholder="Enter Length" class="form-control" />
            </div>

            <div class="col-md-4">
                <label for="">Depth</label>
                <input type="text" name="variant_depth[]" placeholder="Enter Depth" class="form-control" />
            </div>
            <div class="col-md-4">
                <label for="">Diameter</label>
                <input type="text" name="variant_diameter[]" placeholder="Enter Diameter" class="form-control" />
            </div>
            <div class="col-md-4">
                <label for="">Weight</label>
                <input type="text" name="variant_weight[]" placeholder="Enter Weight" class="form-control" />
            </div>

            <div class="col-md-4">
                <label for="">Volume</label>
                <input type="text" name="variant_volume[]" placeholder="Enter Volume" class="form-control" />
            </div>
            <div class="col-md-4">
                <label for="">Price</label>
                <input type="text" name="variant_price[]" placeholder="Enter Price" class="form-control" />
            </div>
            <div class="col-md-4">
                <label for="">Discount</label>
                <input type="text" name="variant_discount[]" placeholder="Enter Discount" class="form-control" />
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Select weight unit</label><br>
                    <select class="form-control  select-search" aria-label=".form-select-lg example"
                        name="variant_w_unit[]" id="w_unit" required>
                        <option selected disabled>Open this select menu</option>
                        @foreach ($product_weight as $weight)
                            <option value="{{ $weight->weight_unit_id }}">
                                {{ $weight->weight_unit_name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Select volume unit</label><br>
                    <select class="form-control  select-search" aria-label=".form-select-lg example"
                        name="variant_v_unit[]" id="v_unit" required>
                        <option selected disabled>Open this select menu</option>
                        @foreach ($product_volume as $volume)
                            <option value="{{ $volume->volume_unit_id }}">
                                {{ $volume->volume_unit_name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Select Diamention unit</label><br>
                    <select class="form-control  select-search" aria-label=".form-select-lg example"
                        name="variant_d_unit[]" id="d_unit" required>
                        <option selected disabled>Open this select menu</option>
                        @foreach ($product_dimention as $dimention)

                            <option value="{{ $dimention->dimension_unit_id }}">
                                {{ $dimention->dimension_unit_name }}</option>

                        @endforeach

                    </select>
                </div>
            </div>

            <div class="col-md-4">

                <label for="colorpicker"> Select Color :</label>
                <input type="color" name="variant_color[]" placeholder="Enter Discount" class="form-control" />
            </div>



        </div>
    </td>





    <td><button type="button" class="btn btn-outline-danger remove-input-field">Delete</button></td>
</tr>
