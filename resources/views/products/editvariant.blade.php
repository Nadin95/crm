@extends('layout.admin_layout')
@section('content')

    <form method="POST" action="{{ url('admin/product/update_product_variants') }}" enctype="multipart/form-data"
        id="image-upload" class="uploader">
        @csrf
        <div class="card">

            <div class="card-header header-elements-inline">
                <h6 class="card-title">Add Varient</h6>

            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="card-body ">


                <table class="table table-bordered" id="dynamicAddRemove">
                    <tr>


                        <th></th>

                    </tr>

                    {{-- @php
                dd($variant);

            @endphp --}}



                    @foreach ($variant as $item)


                        <tr>

                            <td>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">Width</label>
                                        <input class="form-control" value="{{ $item->width }}" name="width"
                                            placeholder="Enter width" type="text" required />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Height</label>
                                        <input type="text" value="{{ $item->height }}" name="height"
                                            placeholder="Enter Height" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Length</label>
                                        <input type="text" value="{{ $item->length }}" name="length"
                                            placeholder="Enter Length" class="form-control" />
                                    </div>

                                    <div class="col-md-4">
                                        <label for="">Depth</label>
                                        <input type="text" value="{{ $item->depth }}" name="depth"
                                            placeholder="Enter Depth" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Diameter</label>
                                        <input type="text" name="diameter" value="{{ $item->diameter }}"
                                            placeholder="Enter Diameter" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Weight</label>
                                        <input type="text" value="{{ $item->weight }}" name="weight"
                                            placeholder="Enter Weight" class="form-control" />
                                    </div>



                                    <div class="col-md-4">
                                        <label for="">Volume</label>
                                        <input type="text" value="{{ $item->volume }}" name="volume"
                                            placeholder="Enter Volume" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Price</label>
                                        <input type="text" value="{{ $item->price }}" name="price"
                                            placeholder="Enter Price" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Discount</label>
                                        <input type="text" value="{{ $item->discount }}" name="discount"
                                            placeholder="Enter Discount" class="form-control" />
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Select weight unit</label><br>
                                            <select class="form-control  select-search" aria-label=".form-select-lg example"
                                                id="w_unit" name="w_unit">
                                                <option selected disabled>Open this select menu</option>
                                                @foreach ($product_weight as $weight)
                                                    <option value="{{ $weight->weight_unit_id }}"
                                                        @if ($weight->weight_unit_id == $item->product_weight_unit_id) selected @endif>
                                                        {{ $weight->weight_unit_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Select volume unit</label><br>
                                            <select class="form-control  select-search" aria-label=".form-select-lg example"
                                                id="v_unit" name="v_unit" required>
                                                <option selected disabled>Open this select menu</option>
                                                @foreach ($product_volume as $volume)
                                                    <option value="{{ $volume->volume_unit_id }}"
                                                        @if ($volume->volume_unit_id == $item->product_volume_unit_id) selected @endif>
                                                        {{ $volume->volume_unit_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Select Diamention unit</label><br>
                                            <select class="form-control  select-search" aria-label=".form-select-lg example"
                                                id="d_unit" name="d_unit" required>
                                                <option selected disabled>Open this select menu</option>
                                                @foreach ($product_dimention as $dimention)

                                                    <option value="{{ $dimention->dimension_unit_id }}"
                                                        @if ($dimention->dimension_unit_id == $item->product_dimension_unit_id) selected @endif>
                                                        {{ $dimention->dimension_unit_name }}</option>

                                                @endforeach

                                            </select>
                                        </div>
                                    </div>


                                </div>
                            </td>




                        </tr>

                    @endforeach

                </table>










            </div>

        </div>



        <div>

            @if (!empty($variant_id))
                <input type="hidden" name="variant_id" value="{{ $variant_id->id }}">
            @endif


            <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                Update Variant
            </button>

        </div>

    </form>



@endsection
