@extends('layout.admin_layout')
@section('content')

    <div class="container">

        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Title</th>
                        <th>Code </th>
                        <th>View</th>
                    </tr>
                </thead>
                <tbody>


                    @foreach ($products as $product)
                        {{-- @php
                            dd($product->product_id);
                        @endphp --}}
                        <tr>
                            <td>{{ $product->product_id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->title }}</td>
                            <td>{{ $product->product_code }}</td>
                            <td>

                                <a class="btn btn-secondary" href="editproduct/{{ $product->product_id }}">View</a><br>
                            </td>
                        </tr>



                    @endforeach





                </tbody>
            </table>
        </div>
    </div>





    </div>




@endsection
