@extends('layout.admin_layout')
@section('content')



    {{-- <div class="col-md-12 "> --}}
    <form method="POST" action="{{ url('admin/product/upload_product') }}" enctype="multipart/form-data" id="image-upload"
        class="uploader">
        @csrf

        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Add Product</h6>

            </div>

            <div class="card-body ">




                <div class="card-body ">

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif



                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Categoeies</label>
                        <div class="col-md-10">
                            <select class="form-control select-search" aria-label=".form-select-lg example" name="cat_id"
                                id="cat_id" required>
                                <option selected disabled>Open this select menu</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Sub Categoey</label>
                        <div class="col-md-10">

                            <select class="form-control select-search" aria-label=".form-select-lg example"
                                name="sub_cat_id" id="sub_cat_id" required>
                                <option selected disabled>Open this select menu</option>
                                @foreach ($subcategories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>

                                @endforeach
                            </select>

                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Sub Sub Category</label>
                        <div class="col-md-10">
                            <select class="form-control  select-search" aria-label=".form-select-lg example"
                                name="subsub_cat_id" id="subsub_cat_id" required>
                                <option selected disabled>Open this select menu</option>

                                @foreach ($sub_sub_categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>




                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Title</label>
                        <div class="col-md-10">
                            <input class="form-control" id="title" name="title" type="text" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Product code </label>
                        <div class="col-md-10">
                            <input class="form-control" id="p_code" name="p_code" type="text" required>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-2" for="">Name</label>
                        <div class="col-md-10">
                            <input class="form-control" id="name" name="name" type="text" required>

                        </div>
                    </div>



                    <div class="form-group row">

                        <label class="col-form-label col-md-2" class="col-form-label col-lg-2">Description</label>
                        <div class="col-lg-10">
                            <textarea rows="3" cols="3" class="form-control" id="des" name="des"
                                placeholder="Default textarea" required></textarea>
                        </div>



                    </div>

                    <div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Upload Images</label>
                            <div class="col-lg-10">

                                <input type="file" id="files" name="files[]" class="form-control-uniform"
                                    data-max-file-size="3M" multiple="multiple" accept="image/*" onchange="" />

                            </div>
                        </div>
                    </div>

                </div>






            </div>

        </div>
        </div>

        <div class="card">

            <div class="card-header header-elements-inline">
                <h6 class="card-title">Add Varient</h6>

            </div>

            <div class="card-body ">


                <table class="table table-bordered" id="dynamicAddRemove">
                    <tr>


                        <th></th>
                        <th>Action</th>
                    </tr>
                    <tr>

                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="">Width</label>
                                    <input class="form-control" name="variant_width[]" placeholder="Enter width"
                                        type="text" required />
                                </div>
                                <div class="col-md-4">
                                    <label for="">Height</label>
                                    <input type="text" name="variant_height[]" placeholder="Enter Height"
                                        class="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <label for="">Length</label>
                                    <input type="text" name="variant_length[]" placeholder="Enter Length"
                                        class="form-control" />
                                </div>

                                <div class="col-md-4">
                                    <label for="">Depth</label>
                                    <input type="text" name="variant_depth[]" placeholder="Enter Depth"
                                        class="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <label for="">Diameter</label>
                                    <input type="text" name="variant_diameter[]" placeholder="Enter Diameter"
                                        class="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <label for="">Weight</label>
                                    <input type="text" name="variant_weight[]" placeholder="Enter Weight"
                                        class="form-control" />
                                </div>



                                <div class="col-md-4">
                                    <label for="">Volume</label>
                                    <input type="text" name="variant_volume[]" placeholder="Enter Volume"
                                        class="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <label for="">Price</label>
                                    <input type="text" name="variant_price[]" placeholder="Enter Price"
                                        class="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <label for="">Discount</label>
                                    <input type="text" name="variant_discount[]" placeholder="Enter Discount"
                                        class="form-control" />
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Select weight unit</label><br>
                                        <select class="form-control  select-search" aria-label=".form-select-lg example"
                                            id="w_unit" name="variant_w_unit[]" required>
                                            <option selected disabled>Open this select menu</option>
                                            @foreach ($product_weight as $weight)
                                                <option value="{{ $weight->weight_unit_id }}">
                                                    {{ $weight->weight_unit_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Select volume unit</label><br>
                                        <select class="form-control  select-search" aria-label=".form-select-lg example"
                                            id="v_unit" name="variant_v_unit[]" required>
                                            <option selected disabled>Open this select menu</option>
                                            @foreach ($product_volume as $volume)
                                                <option value="{{ $volume->volume_unit_id }}">
                                                    {{ $volume->volume_unit_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Select Diamention unit</label><br>
                                        <select class="form-control  select-search" aria-label=".form-select-lg example"
                                            id="d_unit" name="variant_d_unit[]" required>
                                            <option selected disabled>Open this select menu</option>
                                            @foreach ($product_dimention as $dimention)

                                                <option value="{{ $dimention->dimension_unit_id }}">
                                                    {{ $dimention->dimension_unit_name }}</option>

                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">

                                    <label for="colorpicker"> Select Color :</label>
                                    <input type="color" name="variant_color[]" placeholder="Enter Discount"
                                        class="form-control" />
                                </div>


                            </div>
                        </td>


                        <td><button type="button" name="add" id="dynamic-ar" class="btn btn-outline-primary"
                                onclick="admin/product.addvarient()">AddVariant</button>

                        </td>
                    </tr>
                </table>










            </div>

        </div>



        <div>

            @if (!empty($product))
                <input type="hidden" name="product_id" value="{{ $product->product_id + 1 }}">
            @else
                <input type="hidden" name="product_id" value="1">
            @endif

            <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                Upload Product
            </button>

        </div>


    </form>
    {{-- <button type="button" name="add" id="dynamic-ar" class="btn btn-outline-primary"
        onclick="admin/product.addvarient()">AddVarient</button> --}}

@endsection
