@extends('layout.admin_layout')
@section('content')



    {{-- <div class="col-md-12 "> --}}

    {{-- <form method="POST" enctype="multipart/form-data" id="upload-file" action="{{ url('admin/grn/creategrn') }}">
        @csrf --}}
    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Add GRN </h6>

        </div>

        <div class="card-body ">

            {{-- grn_number
            grn_title
            grn_description
            grn_created_by
            quentity
            supplier --}}


            {{-- @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}




            <div class="card-body ">



                <div class="form-group row">
                    <label class="col-form-label col-md-2" for="">Title</label>
                    <div class="col-md-10">
                        <input class="form-control" id="grn_title" name="grn_title" type="text" required>
                        <span class="text-danger" id="title_error"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-md-2" for="">Description</label>
                    <div class="col-md-10">
                        <input class="form-control" id="grn_description" name="grn_description" type="text" required>
                        <span class="text-danger" id="description_error"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-2" for="">Quentity</label>
                    <div class="col-md-10">
                        <input class="form-control" id="quentity" name="quentity" type="text" required>
                        <span class="text-danger" id="quertity_error"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-md-2" for="">Supplier</label>
                    <div class="col-md-10">
                        <input class="form-control" id="supplier" name="supplier" type="text" required>
                        <span class="text-danger" id="supplier_error"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-md-2" for="">Approved status</label>

                    <div class="col-md-10">
                        <div class="custom-control custom-switch">

                            <input type="checkbox" class="custom-control-input" name="approved_status[]"
                                id="approved_status">

                            <label class="custom-control-label" for="approved_status"></label>

                        </div>
                    </div>

                </div>



            </div>






        </div>

    </div>
    </div>


    <div>



        {{-- <button type="submit" name="submit" class="btn btn-primary btn-block mt-4"> --}}
        <button type="submit" onclick="admin/grn.create()" name="submit" class="btn btn-primary btn-block mt-4">
            Add GRN
        </button>

    </div>



    {{-- </form> --}}



    {{-- <button type="button" name="add" id="dynamic-ar" class="btn btn-outline-primary"
        onclick="admin/product.addvarient()">AddVarient</button> --}}

@endsection
