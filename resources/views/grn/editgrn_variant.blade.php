@extends('layout.admin_layout')
@section('content')


    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Add variants</h6>
        </div>
        <div class="card-body ">



            <div class="alert alert-success" style="display:none">
                {{ Session::get('success') }}
            </div>



            <div class="form-group row">
                <label class="col-form-label col-md-2" for="">GRN Id</label>
                <div class="col-md-10">
                    <input class="form-control" id="grn_code" name="grn code" value="{{ $grn_details->grn_number }}"
                        type="text" disabled>
                    <span class="text-danger" id="grn_code_error"></span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-md-2" for="">Product Variant</label>
                <div class="col-md-10">
                    <input class="form-control" id="variant" name="variant" type="text" required>
                    <span class="text-danger" id="variant_error"></span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-md-2" for="">Quantity</label>
                <div class="col-md-10">
                    <input class="form-control" id="quantity" name="quantity" type="text" required>
                    <span class="text-danger" id="quantity_error"></span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-md-2" for="">Unit Price</label>
                <div class="col-md-10">
                    <input class="form-control" id="unit_price" name="unit_price" type="text" required>
                    <span class="text-danger" id="unit_price_error"></span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-md-2" for="">Processed</label>

                <div class="col-md-10">
                    <div class="custom-control custom-switch">

                        <input type="checkbox" class="custom-control-input" name="processed[]" id="processed">

                        <label class="custom-control-label" for="processed"></label>

                    </div>
                </div>

            </div>



            <button type="submit" onclick="admin/grn.variantadd()" name="submit" class="btn btn-primary btn-block mt-4">
                Add variant
            </button>




        </div>

    </div>

@endsection
