@extends('layout.admin_layout')
@section('content')


    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">view GRN </h6>

        </div>

        <div class="card-body ">

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>

                            <th>GRN ID</th>
                            <th>GRN Code</th>
                            <th>Title</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($grn_details as $grn)
                            <tr>
                                <td>{{ $grn->id }}</td>
                                <td>{{ $grn->grn_number }}</td>
                                <td>{{ $grn->grn_title }}</td>
                                <td>
                                    <button type="button" class="btn btn-primary btn-sm"
                                        onclick="admin/grn.viewEditgrn({{ $grn->id }})">View</button>


                                </td>
                            </tr>

                        @endforeach




                    </tbody>
                </table>
            </div>




        </div>
    </div>


@endsection
