@extends('layout.admin_layout')
@section('content')



    <!-- Page content -->
    <div class="page-content">


        <div class="card-body">


            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Image</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sub_subcatdetails as $details)
                            <tr>
                                <td>{{ $details->id }}</td>
                                <td>{{ $details->name }}</td>
                                <td>{{ $details->description }}</td>
                                <td><img src="{{ asset($details->image) }}" height="30px" width="30px"></td>
                            </tr>

                        @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </div>




    </div>
    <!-- /Page content -->




@endsection
