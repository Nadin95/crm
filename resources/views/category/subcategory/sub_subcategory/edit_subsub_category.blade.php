@extends('layout.admin_layout')
@section('content')




    <!-- Page content -->
    {{-- <div class="page-content"> --}}

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Update sub sub Category</h6>




            <button type="submit" class="btn btn-danger"
                onclick="admin/category.delete_subsub_category({{ $sub_subcategory_details->id }})">Remove</button>
        </div>

        <div class="form-group ">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>




        <form method="POST" enctype="multipart/form-data" id="upload-file"
            action="{{ url('admin/category/subcategory/subsubcategory/updatecategory') }}">
            @csrf

            <div class="card-body">
                <form action="#">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Name</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="{{ $sub_subcategory_details->name }}"
                                        name="ssc_name">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Description</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control"
                                        value="{{ $sub_subcategory_details->description }}" name="ssc_dis">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Select categoey </label>
                        <div class="col-lg-10">
                            <select class="mdb-select md-form" aria-label=".form-select-lg example" name="sub_category_id">
                                <option selected disabled>Open this select menu</option>
                                @foreach ($sub_category_details as $item)
                                    <option value="{{ $item->id }}" @if ($item->id == $sub_subcategory_details->parent_id) selected @endif>{{ $item->name }}
                                    </option>

                                @endforeach


                            </select>
                        </div>
                    </div>






                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Image</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="file" id="files" name="files[]" class="dropify"
                                        data-max-file-size-preview="3M" multiple="multiple" accept="image/*" onchange="" />
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="text-right">
                        <input type="hidden" name="h_id" value="{{ $sub_subcategory_details->id }}">

                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>

                    </div>
                </form>
            </div>
    </div>





    {{-- </div> --}}
    <!-- /Page content -->




@endsection
