@extends('layout.admin_layout')
@section('content')




    <!-- Page content -->
    {{-- <div class="page-content"> --}}

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Update Category</h6>


            <button type="submit" class="btn btn-danger"
                onclick="admin/category.deletecategory({{ $category_detail->id }})">Remove</button>
        </div>

        <div class="form-group ">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>


        <form method="POST" enctype="multipart/form-data" id="upload-file"
            action="{{ url('admin/category/updatecategory') }}">
            @csrf

            <div class="card-body">
                <form action="#">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Name</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="{{ $category_detail->name }}"
                                        name="category_name">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Description</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="{{ $category_detail->description }}"
                                        name="category_description">
                                </div>
                            </div>
                        </div>
                    </div>






                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Image</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="file" id="files" name="files[]" class="dropify"
                                        data-max-file-size-preview="3M" multiple="multiple" accept="image/*" onchange="" />
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="text-right">
                        <input type="hidden" name="h_id" value="{{ $category_detail->id }}">

                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>

                    </div>
                </form>
            </div>
    </div>





    {{-- </div> --}}
    <!-- /Page content -->




@endsection
