@extends('layout.admin_layout')
@section('content')





    {{-- create category --}}
    {{-- <!-- Row label (horizontal form) --> --}}
    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Crate Category</h6>

        </div>

        <form method="POST" enctype="multipart/form-data" id="upload-file"
            action="{{ url('admin/category/Savecategory') }}">
            @csrf

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <div class="card-body">
                {{-- <form action="#"> --}}
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Category Name</label>
                    <div class="col-lg-10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="Enter Category Name"
                                    name="categoey_name">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Category Description</label>
                    <div class="col-lg-10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="Enter Category Description"
                                    name="category_description">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Category Image</label>
                    <div class="col-lg-10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="file" id="files" name="files[]" class="dropify"
                                    data-max-file-size-preview="3M" multiple="multiple" accept="image/*" onchange="" />
                            </div>
                        </div>
                    </div>
                </div>


                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
        </form>
    </div>
    </div>


    {{-- /create category --}}


    {{-- create sub category --}}

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Add Sub Category</h6>

        </div>

        <form method="POST" enctype="multipart/form-data" id="upload-file"
            action="{{ url('admin/category/subcategory/savesubcategory') }}">
            @csrf

            <div class="card-body">
                <form action="#">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Name</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Enter Category Name"
                                        name="categoey_name">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Description</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Enter Category Description"
                                        name="category_description">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Select categoey </label>
                        <div class="col-lg-10">
                            <select class="form-control select-search" aria-label=".form-select-lg example"
                                name="category_id">
                                <option selected disabled>Open this select menu</option>
                                @foreach ($category as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>

                                @endforeach


                            </select>
                        </div>
                    </div>




                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Image</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="file" id="files" name="files[]" class="dropify"
                                        data-max-file-size-preview="3M" multiple="multiple" accept="image/*" onchange="" />
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
    </div>

    {{-- create sub category --}}


    {{-- create subsub_subcategory --}}

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Add Category to sub Category</h6>

        </div>

        <form method="POST" enctype="multipart/form-data" id="upload-file"
            action="{{ url('admin/category/subcategory/subsubcategory/save_subsub_category') }}">
            @csrf



            <div class="card-body">
                <form action="#">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Name</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Enter Category Name"
                                        name="ssc_name">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Description</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Enter Category Description"
                                        name="ssc_description">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Select sub categoey </label>
                        <div class="col-lg-10">
                            <select class="form-control select-search" aria-label=".form-select-lg example" name="sc_id">
                                <option selected disabled>Open this select menu</option>
                                @foreach ($subcategories as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>

                                @endforeach


                            </select>
                        </div>
                    </div>




                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Category Image</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="file" id="files" name="files[]" class="dropify"
                                        data-max-file-size-preview="3M" multiple="multiple" accept="image/*" onchange="" />
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
    </div>







@endsection
