@extends('layout.admin_layout')
@section('content')


    <style>


    </style>



    <!-- Page content -->
    <div class="page-content">

        <div class="tree container">



            <ul id="category">

                @foreach ($category as $item)

                    <li data-icon-cls="fa fa-folder" class="panel">

                        <div class="">
                            {{-- <i class="fas fa-plus-circle"></i> --}}
                            <strong>
                                <p data-toggle="collapse" data-target="#cat-{{ $item->id }}" href="#">
                                    {{ $item->name }}
                                    <a href="category/view/{{ $item->id }}"><i class="fas fa-eye mr-3 fa-2x"></i></a>
                                    <a href="category/update_view/{{ $item->id }}"><i
                                            class="fas fa-edit mr-3 fa-2x"></i></a>

                                </p>
                            </strong>


                        </div>



                        <ul id="cat-{{ $item->id }}" class="collapse">

                            @if ($item->subcategories->isNotEmpty())
                                @foreach ($item->subcategories as $subcategory)

                                    <li>
                                        <strong>
                                            <p href="#" data-toggle="collapse" data-target="#subc{{ $subcategory->id }}">
                                                {{ $subcategory->name }}

                                                <a href="category/subcategory/view/{{ $subcategory->id }}"><i
                                                        class="fas fa-eye mr-3 fa-2x"></i></a>
                                                <a href="category/subcategory/update_view/{{ $subcategory->id }}"><i
                                                        class="fas fa-edit mr-3 fa-2x"></i></a>
                                            </p>
                                        </strong>


                                        <ul id="subc{{ $subcategory->id }}" class="collapse">
                                            @if ($subcategory->Su_subbCategoey->isNotEmpty())
                                                @foreach ($subcategory->Su_subbCategoey as $subsub)
                                                    <li>
                                                        <strong>
                                                            <p href="">{{ $subsub->name }}

                                                                <a
                                                                    href="category/subcategory/subsubcategory/view/{{ $subsub->id }}"><i
                                                                        class="fas fa-eye mr-3 fa-2x"></i></a>
                                                                <a
                                                                    href="category/subcategory/subsubcategory/update_view/{{ $subsub->id }}"><i
                                                                        class="fas fa-edit mr-3 fa-2x"></i></a>
                                                            </p>
                                                        </strong>
                                                    </li>



                                                @endforeach
                                            @endif
                                        </ul>
                                    </li>
                                @endforeach

                            @endif






                        </ul>
                    </li>
                @endforeach

            </ul>
        </div>


    </div>





@endsection
