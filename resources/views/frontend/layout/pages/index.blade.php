@extends('frontend.layout.main')

@section('content')

    <!-- Slider Arae Start -->
    <div class="slider-area">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
                    aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                    aria-label="Slide 2"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="assets_front/images/slider-image/sample-1.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets_front/images/slider-image/sample-2.jpg" class="d-block w-100" alt="...">
                </div>

            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
    <!-- Slider Arae End -->

    <section class="category-home">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="text-center">Shop by category for all your sewing supplies</h2>
                </div>

                @foreach ($categories as $category)

                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-6 bottom-spacing">

                        <a href="{{ url('/all_category') }}">
                            <img src="{{ $category->image }}" alt="" width="300px" height="200px">
                            <p class="text-center">{{ $category->name }}</p>
                        </a>
                    </div>

                @endforeach


                <div class="col-xs-12 text-center pt-2">
                    <a class="btn btn-red">View more</a>
                </div>
            </div>
        </div>
    </section>

    <!-- Best Sells Area Start -->
    <section class="best-sells-area mb-30px">
        <div class="container">
            <!-- Section Title Start -->
            <div class="row">
                <div class="col-md-8 col-8">
                    <div class="section-title">
                        <h2>Best Sellers</h2>
                        <p>Add bestselling products to weekly line up</p>
                    </div>
                </div>
                <div class="col-md-4 col-4 view-more-div">
                    <a href="" class="btn btn-border">View more</a>
                </div>
            </div>
            <!-- Section Title End -->
            <!-- Best Sell Slider Carousel Start -->
            <div class="best-sell-slider owl-carousel owl-nav-style">
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-1.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-1.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">Juicy Couture Juicy Quilted Ter..</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price">€18.90</li>
                                <li class="current-price">€34.21</li>
                                <li class="discount-price">-5%</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-2.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-15.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">New Balance Fresh Foam Ka..</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price">€18.90</li>
                                <li class="current-price">€15.12</li>
                                <li class="discount-price">-20%</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-3.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-4.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">Brixton Patrol All Terrain Ano..</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price not-cut">€18.90</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-5.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-5.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">Juicy Couture Tricot Logo Strip..</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price not-cut">€18.90</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-6.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-6.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">New Balance Arishi Sport v1</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price not-cut">€18.90</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-7.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-8.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">Fila Locker Room Varsity Jacket</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price not-cut">€18.90</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-9.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-9.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">Water and Wind Resistant Ins..</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price not-cut">€18.90</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-10.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-10.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">New Luxury Men's Slim Fit Shi...</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price not-cut">€29.90</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-11.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-12.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">Originals Kaval Windbreaker...</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price">€23.90</li>
                                <li class="current-price">€21.51</li>
                                <li class="discount-price">-10%</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-13.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-3.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">Brixton Patrol All Terrain Anor...</a>
                        </h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price not-cut">€18.90</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-14.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-14.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">Madden by Steve Madden Cale 6</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price">€11.90</li>
                                <li class="current-price">€10.12</li>
                                <li class="discount-price">-15%</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>

                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
                <article class="list-product">
                    <div class="img-block">
                        <a href="single-product.html" class="thumbnail">
                            <img class="first-img" src="assets_front/images/product-image/organic/product-15.jpg"
                                alt="" />
                            <img class="second-img" src="assets_front/images/product-image/organic/product-2.jpg"
                                alt="" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>

                    <div class="product-decs">

                        <h2><a href="single-product.html" class="product-link">Juicy Couture Juicy Quilted Ter..</a></h2>
                        <div class="rating-product">
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                            <i class="ion-android-star"></i>
                        </div>
                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price">€35.90</li>
                                <li class="current-price">€34.11</li>
                                <li class="discount-price">-5%</li>
                            </ul>
                        </div>
                    </div>
                    <div class="add-to-link">
                        <ul>
                            <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a></li>
                            <li>
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </li>
                            <li>
                                <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                            </li>
                        </ul>
                    </div>
                </article>
                <!-- Single Item -->
            </div>
            <!-- Best Sells Carousel End -->
        </div>
    </section>
    <!-- Best Sells Slider End -->

    <section class="special-offers">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-8">
                    <h2>Check out these special offers and save more! </h2>
                </div>
                <div class="col-md-4 col-4 see-all">

                    <a href="" class="btn btn-border">See all specials</a>
                </div>
                <div class="special-offers-wrapper owl-carousel owl-theme">
                    <div class="special-offer-item">
                        <img src="assets_front/images/special-offers/us-06.jpg" alt="">
                    </div>
                    <div class="special-offer-item">
                        <img src="assets_front/images/special-offers/us-07.jpg" alt="">
                    </div>
                    <div class="special-offer-item">
                        <img src="assets_front/images/special-offers/us-06.jpg" alt="">
                    </div>
                    <div class="special-offer-item">
                        <img src="assets_front/images/special-offers/us-07.jpg" alt="">
                    </div>
                    <div class="special-offer-item">
                        <img src="assets_front/images/special-offers/us-06.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Hot deal area Start -->
    <section class="hot-deal-area">
        <div class="container">
            <div class="row">

                <!-- New Arrivals Area Start -->
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-8 col-8">
                            <!-- Section Title -->
                            <div class="section-title ml-0px mt-res-sx-30px">
                                <h2>New Arrivals</h2>
                                <p>Add new products to weekly line up</p>
                            </div>
                            <!-- Section Title -->
                        </div>

                        <div class="col-md-4 col-4 view-more-div">
                            <a href="" class="btn btn-border">View more</a>
                        </div>


                    </div>
                    <!-- New Product Slider Start -->
                    <div class="new-product-slider owl-carousel owl-nav-style">
                        <!-- Product Single Item -->
                        <div class="product-inner-item">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img"
                                            src="assets_front/images/product-image/organic/product-16.jpg" alt="" />
                                        <img class="second-img"
                                            src="assets_front/images/product-image/organic/product-16.jpg" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                            data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="product-decs">

                                    <h2><a href="single-product.html" class="product-link">Originals Kaval Windbr...</a>
                                    </h2>
                                    <div class="rating-product">
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                    </div>
                                    <div class="pricing-meta">
                                        <ul>
                                            <li class="old-price">€23.90</li>
                                            <li class="current-price">€21.51</li>
                                            <li class="discount-price">-10%</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>

                                    </ul>
                                </div>
                            </article>

                        </div>
                        <!-- Product Single Item -->
                        <div class="product-inner-item">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img"
                                            src="assets_front/images/product-image/organic/product-4.jpg" alt="" />
                                        <img class="second-img"
                                            src="assets_front/images/product-image/organic/product-13.jpg" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                            data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="product-decs">

                                    <h2><a href="single-product.html" class="product-link">Brixton Patrol All Terr...</a>
                                    </h2>
                                    <div class="rating-product">
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                    </div>
                                    <div class="pricing-meta">
                                        <ul>
                                            <li class="old-price not-cut">€29.90</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>

                                    </ul>
                                </div>
                            </article>

                        </div>
                        <!-- Product Single Item -->
                        <div class="product-inner-item">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img"
                                            src="assets_front/images/product-image/organic/product-11.jpg" alt="" />
                                        <img class="second-img"
                                            src="assets_front/images/product-image/organic/product-12.jpg" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                            data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="product-decs">

                                    <h2><a href="single-product.html" class="product-link">Originals Kaval Windbr...</a>
                                    </h2>
                                    <div class="rating-product">
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                    </div>
                                    <div class="pricing-meta">
                                        <ul>
                                            <li class="old-price">€35.90</li>
                                            <li class="current-price">€34.11</li>
                                            <li class="discount-price">-5%</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>

                                    </ul>
                                </div>
                            </article>

                        </div>
                        <!-- Product Single Item -->
                        <div class="product-inner-item">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img"
                                            src="assets_front/images/product-image/organic/product-14.jpg" alt="" />
                                        <img class="second-img"
                                            src="assets_front/images/product-image/organic/product-14.jpg" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                            data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="product-decs">

                                    <h2><a href="single-product.html" class="product-link">Madden by Steve Madd...</a>
                                    </h2>
                                    <div class="rating-product">
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                    </div>
                                    <div class="pricing-meta">
                                        <ul>
                                            <li class="old-price">€11.90</li>
                                            <li class="current-price">€10.12</li>
                                            <li class="discount-price">-15%</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>

                                    </ul>
                                </div>
                            </article>

                        </div>
                        <!-- Product Single Item -->
                        <div class="product-inner-item">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img"
                                            src="assets_front/images/product-image/organic/product-9.jpg" alt="" />
                                        <img class="second-img"
                                            src="assets_front/images/product-image/organic/product-9.jpg" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                            data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="product-decs">

                                    <h2><a href="single-product.html" class="product-link">Water and Wind Resist...</a>
                                    </h2>
                                    <div class="rating-product">
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                    </div>
                                    <div class="pricing-meta">
                                        <ul>
                                            <li class="old-price not-cut">€11.90</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>

                                    </ul>
                                </div>
                            </article>

                        </div>
                        <!-- Product Single Item -->
                        <div class="product-inner-item">
                            <article class="list-product mb-30px">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img"
                                            src="assets_front/images/product-image/organic/product-18.jpg" alt="" />
                                        <img class="second-img"
                                            src="assets_front/images/product-image/organic/product-18.jpg" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                            data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="product-decs">

                                    <h2><a href="single-product.html" class="product-link">Juicy Couture Solid
                                            Slee...</a></h2>
                                    <div class="rating-product">
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                    </div>
                                    <div class="pricing-meta">
                                        <ul>
                                            <li class="old-price not-cut">€18.90</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        <li class="cart"><a class="cart-btn" href="#">ADD TO CART </a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>

                                    </ul>
                                </div>
                            </article>

                        </div>
                    </div>
                    <!-- Product Slider End -->
                </div>
            </div>
        </div>
    </section>
    <!-- Hot Deal Area End -->
    <!-- Banner Area Start -->
    <div class="banner-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <div class="banner-wrapper">
                        <a href="shop-4-column.html"><img src="assets_front/images/banner-image/1.jpg" alt="" /></a>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 mt-res-sx-30px">
                    <div class="banner-wrapper">
                        <a href="shop-4-column.html"><img src="assets_front/images/banner-image/2.jpg" alt="" /></a>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12 mt-res-sx-30px">
                    <div class="banner-wrapper">
                        <a href="shop-4-column.html"><img src="assets_front/images/banner-image/3.jpg" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Area End -->
    <!-- Feature Area Start -->
    <section class="feature-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-8">
                    <!-- Section Title -->
                    <div class="section-title">
                        <h2>Fresh fruits</h2>
                    </div>
                    <!-- Section Title -->
                </div>
                <div class="col-md-4 col-4 view-more-div">
                    <a href="" class="btn btn-border">View more</a>
                </div>
            </div>
            <!-- Feature Slider Start -->
            <div class="feature-slider owl-carousel owl-nav-style">
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-18.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-18.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Juicy Couture Solid...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price not-cut">€29.90</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-16.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-17.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Originals Kaval Win...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€23.90</li>
                                    <li class="current-price">€21.51</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-2.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-15.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Juicy Couture Juicy...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€35.90</li>
                                    <li class="current-price">€34.11</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-2.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-23.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">New Balance Fresh...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€18.90</li>
                                    <li class="current-price">€15.12</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-5.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-5.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Juicy Couture Trico...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price not-cut">€9.90</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-17.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-16.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Trans-Weight Hood...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€18.90</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-22.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-15.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Calvin Klein Jeans...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price not-cut">€29.90</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-14.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-14.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Madden by Steve...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€11.90</li>
                                    <li class="current-price">€10.12</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-1.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-1.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Juicy Couture Juicy...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€18.90</li>
                                    <li class="current-price">€34.21</li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Single Item -->
            </div>
            <!-- Feature Slider End -->
        </div>
    </section>
    <section class="feature-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-8">
                    <!-- Section Title -->
                    <div class="section-title">
                        <h2>Fresh Vegetables</h2>
                    </div>
                    <!-- Section Title -->
                </div>
                <div class="col-md-4 col-4 view-more-div">
                    <a href="" class="btn btn-border">View more</a>
                </div>
            </div>
            <!-- Feature Slider Start -->
            <div class="feature-slider owl-carousel owl-nav-style">
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-18.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-18.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Juicy Couture Solid...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price not-cut">€29.90</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-16.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-17.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Originals Kaval Win...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€23.90</li>
                                    <li class="current-price">€21.51</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-2.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-15.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Juicy Couture Juicy...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€35.90</li>
                                    <li class="current-price">€34.11</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-2.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-23.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">New Balance Fresh...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€18.90</li>
                                    <li class="current-price">€15.12</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-5.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-5.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Juicy Couture Trico...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price not-cut">€9.90</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-17.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-16.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Trans-Weight Hood...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€18.90</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-22.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-15.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Calvin Klein Jeans...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price not-cut">€29.90</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-14.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-14.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Madden by Steve...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€11.90</li>
                                    <li class="current-price">€10.12</li>
                                </ul>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- Single Item -->
                <div class="feature-slider-item">
                    <article class="list-product">
                        <div class="img-block">
                            <a href="single-product.html" class="thumbnail">
                                <img class="first-img" src="assets_front/images/product-image/organic/product-1.jpg"
                                    alt="" />
                                <img class="second-img" src="assets_front/images/product-image/organic/product-1.jpg"
                                    alt="" />
                            </a>
                            <div class="quick-view">
                                <a class="quick_view" href="#" data-link-action="quickview" title="Quick view"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-decs">

                            <h2><a href="single-product.html" class="product-link">Juicy Couture Juicy...</a></h2>
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price">€18.90</li>
                                    <li class="current-price">€34.21</li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Single Item -->
            </div>
            <!-- Feature Slider End -->
        </div>
    </section>
    <!-- Feature Area End -->
    <!-- Static Area Start -->
    <section class="static-area">
        <div class="container">
            <div class="static-area-wrap">
                <div class="row">
                    <!-- Static Single Item Start -->
                    <div class="col-lg-3 col-xs-12 col-md-6 col-sm-6">
                        <div class="single-static pb-res-md-0 pb-res-sm-0 pb-res-xs-0">
                            <img src="assets_front/images/icons/static-icons-1.png" alt="" class="img-responsive" />
                            <div class="single-static-meta">
                                <h4>Free Shipping</h4>
                                <p>On all orders over $75.00</p>
                            </div>
                        </div>
                    </div>
                    <!-- Static Single Item End -->
                    <!-- Static Single Item Start -->
                    <div class="col-lg-3 col-xs-12 col-md-6 col-sm-6">
                        <div class="single-static pb-res-md-0 pb-res-sm-0 pb-res-xs-0 pt-res-xs-20">
                            <img src="assets_front/images/icons/static-icons-2.png" alt="" class="img-responsive" />
                            <div class="single-static-meta">
                                <h4>Free Returns</h4>
                                <p>Returns are free within 9 days</p>
                            </div>
                        </div>
                    </div>
                    <!-- Static Single Item End -->
                    <!-- Static Single Item Start -->
                    <div class="col-lg-3 col-xs-12 col-md-6 col-sm-6">
                        <div class="single-static pt-res-md-30 pb-res-sm-30 pb-res-xs-0 pt-res-xs-20">
                            <img src="assets_front/images/icons/static-icons-3.png" alt="" class="img-responsive" />
                            <div class="single-static-meta">
                                <h4>100% Payment Secure</h4>
                                <p>Your payment are safe with us.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Static Single Item End -->
                    <!-- Static Single Item Start -->
                    <div class="col-lg-3 col-xs-12 col-md-6 col-sm-6">
                        <div class="single-static pt-res-md-30 pb-res-sm-30 pt-res-xs-20">
                            <img src="assets_front/images/icons/static-icons-4.png" alt="" class="img-responsive" />
                            <div class="single-static-meta">
                                <h4>Support 24/7</h4>
                                <p>Contact us 24 hours a day</p>
                            </div>
                        </div>
                    </div>
                    <!-- Static Single Item End -->
                </div>
            </div>
        </div>
    </section>
    <!-- Static Area End -->
    <!-- Large banner area start -->
    <section class="bottom-banners">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <img src="assets_front/images/bottom-banners/img1.jpg" class="banner-lg-1" alt="">
                    <img src="assets_front/images/bottom-banners/img3.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <img src="assets_front/images/bottom-banners/img0.jpg" class="" alt="">

                </div>
                <div class="col-md-3">
                    <img src="assets_front/images/bottom-banners/img1.jpg" class="banner-lg-1" alt="">
                    <img src="assets_front/images/bottom-banners/img3.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- Large banner area end -->
    <!-- Brand area start -->
    <div class="brand-area">
        <div class="container">
            <div class="brand-slider owl-carousel owl-nav-style owl-nav-style-2">
                <div class="brand-slider-item">
                    <a href="#">
                        <img src="assets_front/images/brand-logo/img6.jpg" class="brand-logo1" alt="" />
                        <img src="assets_front/images/brand-logo/img5.jpg" alt="" />
                    </a>
                </div>
                <div class="brand-slider-item">
                    <a href="#">
                        <img src="assets_front/images/brand-logo/img2.jpg" class="brand-logo1" alt="" />
                        <img src="assets_front/images/brand-logo/img7.jpg" alt="" />
                    </a>
                </div>
                <div class="brand-slider-item">
                    <a href="#">
                        <img src="assets_front/images/brand-logo/img3.jpg" class="brand-logo1" alt="" />
                        <img src="assets_front/images/brand-logo/img1.jpg" alt="" />
                    </a>
                </div>
                <div class="brand-slider-item">
                    <a href="#">
                        <img src="assets_front/images/brand-logo/img4.jpg" class="brand-logo1" alt="" />
                        <img src="assets_front/images/brand-logo/img6.jpg" alt="" />
                    </a>
                </div>
                <div class="brand-slider-item">
                    <a href="#">
                        <img src="assets_front/images/brand-logo/img5.jpg" class="brand-logo1" alt="" />
                        <img src="assets_front/images/brand-logo/img2.jpg" alt="" />
                    </a>
                </div>
                <div class="brand-slider-item">
                    <a href="#">
                        <img src="assets_front/images/brand-logo/img1.jpg" class="brand-logo1" alt="" />
                        <img src="assets_front/images/brand-logo/img4.jpg" alt="" />
                    </a>
                </div>
                <div class="brand-slider-item">
                    <a href="#">
                        <img src="assets_front/images/brand-logo/img7.jpg" class="brand-logo1" alt="" />
                        <img src="assets_front/images/brand-logo/img3.jpg" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Brand area end -->

    {{-- <H1>Hi</H1> --}}

@endsection
