@extends('frontend.layout.main')

@section('content')


    {{-- <h1>Gi</h1> --}}

    <!-- Breadcrumb Area start -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-content">
                        <h1 class="breadcrumb-hrading">Sewing Thread</h1>
                        <ul class="breadcrumb-links">
                            <li><a href="index.html">Home</a></li>
                            <li>Sewing Thread</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->
    <!-- Shop Category Area End -->
    <div class="shop-category-area shop-category-area-main">
        <div class="container">
            <div class="row">

                {{-- @php
                    dd($categories);
                @endphp --}}

                {{-- @if (count($categories) < 1) --}}


                @foreach ($categories as $category)
                    <div class="category-header d-flex justify-content-between">
                        <h2>{{ $category->name }}</h2>
                        <a href="" class="btn btn-border">View more</a>
                    </div>

                    @foreach ($products as $product)
                        {{-- @php
                            dd($product);
                        @endphp --}}

                        @if ($product->category_id == $category->id)


                            <div class="category-block-wrapper d-flex">
                                @foreach ($product_images as $image)

                                    @if ($image->product_id == $product->product_id)
                                        <div class="category-block-item text-center category-sub">

                                            <a href="{{ url('/item/display/' . $image->product_id) }}">
                                                <img src="{{ $image->image }}" alt="" width="100px" height="100px">
                                                <p>{{ $image->name }}</p>
                                            </a>
                                        </div>
                                    @endif




                                @endforeach
                            </div>



                        @endif

                    @endforeach





                    <div class="category-block-wrapper d-flex">
                        <div class="category-block-item text-center category-sub">
                            <a href="">
                                <img src="" alt="">
                                <p></p>
                            </a>
                        </div>
                    </div>




                @endforeach

                {{-- @endif --}}




                {{-- <div class="category-header d-flex justify-content-between">
                    <h2>SEWING THREAD BY MATERIAL</h2>
                    <a href="" class="btn btn-border">View more</a>
                </div>

                <div class="category-block-wrapper d-flex">
                    <div class="category-block-item text-center category-sub">
                        <a href="">
                            <img src="assets_front/images/category-sub/a-e-thread.jpg" alt="">
                            <p>A&E THREAD</p>
                        </a>
                    </div>
                    <div class="category-block-item text-center category-sub">
                        <a href="">
                            <img src="assets_front/images/category-sub/amann-thread.jpg" alt="">
                            <p>A&E THREAD</p>
                        </a>
                    </div>
                    <div class="category-block-item text-center category-sub">
                        <a href="">
                            <img src="assets_front/images/category-sub/coats-and-clark-threadrev.jpg" alt="">
                            <p>A&E THREAD</p>
                        </a>
                    </div>
                    <div class="category-block-item text-center category-sub">
                        <a href="">
                            <img src="assets_front/images/category-sub/isacord-thread.jpg" alt="">
                            <p>A&E THREAD</p>
                        </a>
                    </div>
                    <div class="category-block-item text-center category-sub">
                        <a href="">
                            <img src="assets_front/images/category-sub/amann-thread.jpg" alt="">
                            <p>A&E THREAD</p>
                        </a>
                    </div>
                    <div class="category-block-item text-center category-sub">
                        <a href="">
                            <img src="assets_front/images/category-sub/isacord-thread.jpg" alt="">
                            <p>A&E THREAD</p>
                        </a>
                    </div>
                    <div class="category-block-item text-center category-sub">
                        <a href="">
                            <img src="assets_front/images/category-sub/a-e-thread.jpg" alt="">
                            <p>A&E THREAD</p>
                        </a>
                    </div>
                    <div class="category-block-item text-center category-sub">
                        <a href="">
                            <img src="assets_front/images/category-sub/amann-thread.jpg" alt="">
                            <p>A&E THREAD</p>
                        </a>
                    </div>
                    <div class="category-block-item text-center category-sub">
                        <a href="">
                            <img src="assets_front/images/category-sub/isacord-thread.jpg" alt="">
                            <p>A&E THREAD</p>
                        </a>
                    </div>

                    <div class="category-block-item text-center category-sub">
                        <a href="">
                            <img src="assets_front/images/category-sub/isacord-thread.jpg" alt="">
                            <p>A&E THREAD</p>
                        </a>
                    </div> --}}


            </div>
        </div>
    </div>
    </div>
    <!-- Shop Category Area End -->


@endsection
