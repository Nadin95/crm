      <!-- Header Start -->
      <header class="main-header">
          <!-- Header Top Start -->
          <div class="header-top-nav">
              <div class="container-fluid">
                  <div class="row">
                      <!--Left Start-->
                      <div class="col-lg-4 col-md-4">
                          <div class="left-text">
                              <p>Welcome you to mahum.lk !</p>
                          </div>
                      </div>
                      <!--Left End-->
                      <!--Right Start-->
                      <div class="col-lg-8 col-md-8 text-right hide-mobile">
                          <div class="header-right-nav">
                              <ul class="res-xs-flex">
                                  <li class="after-n">
                                      <a href="login.html"></i>Login</a>
                                  </li>
                                  <li>
                                      <a href="register.html"></i>Register</a>
                                  </li>
                              </ul>

                          </div>
                      </div>
                      <!--Right End-->
                  </div>
              </div>
          </div>
          <!-- Header Top End -->
          <!-- Header Buttom Start -->
          <div class="header-navigation sticky-nav">
              <div class="container-fluid">
                  <div class="row">
                      <!-- Logo Start -->
                      <div class="col-md-3 col-sm-4 col-12" id="desktop_logo">
                          <div class="logo">
                              <a href="index.html"><img src="assets_front/images/logo/logo.jpg" alt="" /></a>
                          </div>
                      </div>
                      <!-- Logo End -->
                      <!-- Navigation Start -->
                      <div class="col-md-9 col-sm-8 col-12">
                          <!--Main Navigation Start -->
                          <div class="main-navigation d-none d-lg-block">
                              <ul>
                                  <li><a href="/">Home</a></li>
                                  <li class="menu-dropdown">
                                      <a href="#">Categories <i class="fas fa-chevron-down"></i></a>
                                      <ul class="sub-menu">
                                          <li><a href="category.html">Sewing Thread</a></li>

                                      </ul>
                                  </li>
                                  <li><a href="products.html">Products</a></li>
                                  <li><a href="videos.html">Videos</a></li>
                                  <!-- <li><a href="">About us</a></li> -->
                                  <li><a href="contact.html">Contact Us</a></li>
                              </ul>
                          </div>
                          <!--Main Navigation End -->
                          <!--Header Bottom Account Start -->
                          <div class="header_account_area">
                              <!--Seach Area Start -->
                              <div class="header_account_list search_list">
                                  <a href="javascript:void(0)"><i class="ion-ios-search-strong"></i></a>
                                  <div class="dropdown_search">
                                      <form action="#">
                                          <input placeholder="Enter your search key ..." type="text" />
                                          <div class="search-category">
                                              <select class="bootstrap-select" name="poscats">
                                                  <option value="0">All categories</option>
                                                  <option value="104" class="font-bold1">
                                                      Fresh Food
                                                  </option>
                                                  <option value="183">
                                                      - - Fresh Fruit
                                                  </option>
                                                  <option value="187">
                                                      - - Bananas
                                                  </option>
                                                  <option value="188">
                                                      - - Apples &amp; Pears
                                                  </option>

                                              </select>
                                          </div>
                                          <button type="submit"><i class="fas fa-search"></i></button>
                                      </form>
                                  </div>
                              </div>
                              <!--Seach Area End -->
                              <!--Contact info Start -->
                              <div class="contact-link">
                                  <div class="phone">
                                      <p>Call us:</p>
                                      <a href="tel:(+94)755600099">(94) 7556 00099</a>
                                  </div>
                              </div>
                              <!--Contact info End -->
                              <!--Cart info Start -->
                              <div class="cart-info d-flex">
                                  <div class="mini-cart-warp">
                                      <a href="#" class="count-cart">

                                          <span class="item-quantity-tag">02</span>
                                      </a>

                                  </div>
                              </div>
                              <!--Cart info End -->
                          </div>
                      </div>
                  </div>
                  <!-- mobile menu -->
                  <div class="mobile-menu-area">
                      <div class="mobile-menu">
                          <nav id="mobile-menu-active">
                              <ul class="menu-overflow">
                                  <li>
                                      <a href="index.html">HOME</a>

                                  </li>
                                  <li>
                                      <a href="#">Categories</a>
                                      <ul>
                                          <li>
                                              <a href="#">Shop Grid</a>
                                              <!-- <ul>
                                                <li><a href="shop-3-column.html">Shop Grid 3 Column</a></li>
                                                <li><a href="shop-4-column.html">Shop Grid 4 Column</a></li>
                                                <li><a href="shop-left-sidebar.html">Shop Grid Left Sidebar</a></li>
                                                <li><a href="shop-right-sidebar.html">Shop Grid Right Sidebar</a></li>
                                            </ul> -->
                                          </li>
                                          <li>
                                              <a href="#">Products</a>
                                          </li>
                                          <li>
                                              <a href="#">Products</a>
                                          </li>
                                          <li>
                                              <a href="#">Products</a>
                                          </li>
                                          <li>
                                              <a href="#">Products</a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li><a href="contact.html">Products</a></li>
                                  <li><a href="contact.html">Videos</a></li>
                                  <li><a href="contact.html">About us</a></li>

                                  <li><a href="contact.html">Contact Us</a></li>
                              </ul>
                          </nav>
                      </div>
                  </div>
                  <!-- mobile menu end-->
              </div>
          </div>
          <!--Header Bottom Account End -->
      </header>
      <!-- Header End -->
