<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Home - mahum.lk </title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon"
        href="{{ URL::asset('assets_front/images/favicon/favicon.png') }}" />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&display=swap"
        rel="stylesheet" />

    <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets_front/css/plugins/owl.theme.default.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets_front/css/vendor/vendor.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets_front/css/plugins/plugins.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets_front/css/dropify.css') }}">

    <!--===== Main Css Files =====-->
    <link rel="stylesheet" href="{{ URL::asset('assets_front/css/style.css') }}" />
    <!-- ===== Responsive Css Files ===== -->
    <link rel="stylesheet" href="{{ URL::asset('assets_front/css/responsive.css') }}" />

    <!-- CSS STYLE-->
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('assets_front/css/plugins/xzoom.min.css" media="all') }}" />
</head>

<body>
    <!-- main layout start from here -->
    <!--====== PRELOADER PART START ======-->

    {{-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> --}}

    <!--====== PRELOADER PART ENDS ======-->
    <div id="main">
        @include('frontend.layout.componants.header')

        {{-- <div class="content"> --}}

        @yield('content')

        {{-- </div> --}}

        @include('frontend.layout.componants.footer')




    </div>
</body>

</html>
