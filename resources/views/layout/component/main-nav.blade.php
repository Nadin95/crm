<div class="navbar navbar-dark navbar-expand-xl rounded-top">
    <div class="navbar-brand">
        <a href="index.html" class="d-inline-block">
            {{-- <img src="{{ URL::asset('global_assets/images/logo_light.png') }}" alt=""> --}}
            <img src="{{ URL::asset('assets_front/images/logo/logo.jpg') }}" alt="" width="150px" height="100px">



        </a>

    </div>

    <div class="d-xl-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-demo3-mobile">
            <i class="icon-grid3"></i>
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-demo3-mobile">
        <ul class="navbar-nav">
            {{-- <li class="nav-item"><a href="#" class="navbar-nav-link">Link</a></li>
            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Dropdown</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item">Action</a>
                    <a href="#" class="dropdown-item">Another action</a>
                    <a href="#" class="dropdown-item">Something else here</a>
                    <a href="#" class="dropdown-item">One more line</a>
                </div>
            </li> --}}
        </ul>

        <span class="navbar-text ml-xl-3">
            <span class="badge bg-success">Online</span>
        </span>

        <ul class="navbar-nav ml-xl-auto">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link">
                    <i class="icon-bell2"></i>
                    <span class="d-xl-none ml-2">Notifications</span>
                    <span class="badge badge-pill bg-warning-400 float-right float-xl-none">2</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="navbar-nav-link">
                    <i class="icon-bubbles4"></i>
                    <span class="d-xl-none ml-2">Messages</span>
                </a>
            </li>
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ URL::asset('global_assets/images/placeholders/placeholder.jpg') }}"
                        class="rounded-circle mr-2" height="34" alt="">
                    <span> {{ $user_details->name }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="/Signout">Signout</a>


                </div>
            </li>
        </ul>
    </div>
</div>
