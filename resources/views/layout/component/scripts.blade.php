<script src="{{ URL::asset('global_assets/js/main/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/dropify.js') }}"></script>
<script src="{{ URL::asset('assets/js/admin/admin.js') }}"></script>
<script src="{{ URL::asset('assets/js/admin/category/category.js') }}"></script>
<script src="{{ URL::asset('assets/js/admin/product/product.js') }}"></script>
<script src="{{ URL::asset('assets/js/admin/grn/grn.js') }}"></script>
