<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="{{ URL::asset('global_assets/images/placeholders/placeholder.jpg') }}"
                                width=" 38" height="38" class="rounded-circle" alt=""></a>
                        {{-- {{URL::asset('global_assets/js/demo_pages/extra_trees.js')}} --}}
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold">


                            {{ $user_details->name }}

                        </div>
                        <div class="font-size-xs opacity-50">

                            <i class="fas fa-phone-volume mr-3 fa-2x"></i> {{ $user_details->contact_number }}
                        </div>
                    </div>

                    {{-- <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div> --}}
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu"
                        title="Main"></i>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::asset('admin/index') }}" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
                            Dashboard
                        </span>
                    </a>
                </li>
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Admin</div> <i class="icon-menu"
                        title="Main"></i>
                </li>
                {{-- Admin --}}
                <li class="nav-item nav-item-submenu">
                    <a href="" class="nav-link">
                        <span>Admin</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        {{-- <li class="nav-item"><a href="index.html" class="nav-link active">Default layout</a></li> --}}
                        <li class="nav-item"><a href="{{ URL::asset('admin/admin') }}"
                                class=" nav-link">Create Admin</a></li>

                    </ul>
                </li>

                {{-- Categories --}}
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Categoties</div> <i class="icon-menu"
                        title="Main"></i>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"> <span>Categoties</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Themes">
                        <li class="nav-item"><a href="{{ URL::asset('admin/category/') }}"
                                class="nav-link active">Category List</a></li>
                        <li class="nav-item"><a href="{{ URL::asset('admin/category/create') }}"
                                class="nav-link">Create Category</a></li>

                    </ul>
                </li>

                {{-- products --}}
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Products</div> <i class="icon-menu"
                        title="Main"></i>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"> <span>Products</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Themes">
                        <li class="nav-item"><a href="{{ URL::asset('admin/product/allproducts') }}"
                                class="nav-link active">All Products</a></li>
                        <li class="nav-item"><a href="{{ URL::asset('admin/product/addview') }}"
                                class="nav-link">Create Product</a></li>

                    </ul>
                </li>

                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Stock Management</div> <i
                        class="icon-menu" title="Main"></i>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"> <span>Stock Management</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Themes">

                        <li class="nav-item"><a href="{{ URL::asset('admin/product/allproducts') }}"
                                class="nav-link active">GRN List</a></li>
                        <li class="nav-item"><a href="{{ URL::asset('admin/grn/addview') }}"
                                class="nav-link">Create GRN</a></li>

                    </ul>
                </li>


                <!-- /main -->





















                <!-- /page kits -->

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->
