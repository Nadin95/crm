<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CRM</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/colors.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/components.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/layout.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap_limitless.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/dropify.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/dropify.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/style.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/dropify.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('global_assets/css/icons/icomoon/styles.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('global_assets/css/icons/fontawesome/styles.min.css') }}">

    <script href="{{ URL::asset('global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
    {{-- <script src="../../../../global_assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script> --}}





    <link type="text/css" rel="stylesheet" href="{{ URL::asset('global_assets/css/extras/animate.min.css') }}">

    <script src="{{ URL::asset('global_assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/ui/ripple.min.js') }}"></script>
    <!-- Theme JS files -->
    <script src="{{ URL::asset('global_assets/js/plugins/uploaders/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script src="{{ URL::asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    </script>
    <script src="{{ URL::asset('global_assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}" rel="stylesheet"
        type="text/css">
    </script>

    <script src="{{ URL::asset('global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js') }}" rel="stylesheet"
        type="text/css">
    </script>






    {{-- ///////////////////////////////////////////////////////////////////////////////////////// --}}

    {{-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
    <link href="{{URL::asset('global_assets/css/icons/icomoon/styles.min.css')}}" rel="stylesheet" type="text/c/> --}}
    {{-- <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css"> --}}

    <script src="{{ URL::asset('global_assets/js/plugins/extensions/jquery_ui/core.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/extensions/jquery_ui/effects.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/extensions/cookie.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>




    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script src="{{ URL::asset('assets/js/custom.js') }}"></script>
    <script src="{{ URL::asset('assets/js/dropify.js') }}"></script>



    <script src="{{ URL::asset('global_assets/js/demo_pages/extra_trees.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/demo_pages/extra_trees.js') }}"></script>
    <script src="{{ URL::asset('global_assets/js/demo_pages/form_inputs.js') }}"></script>


    <!-- /theme JS files -->







</head>

<body>
    @include('layout.component.main-nav')

    <div class="page-content">

        @include('layout.component.sidebar')


        <!-- Main content -->
        <div class="content-wrapper">

            @include('layout.component.page-header')
            <!-- Content area -->
            <div class="content">

                @yield('content')

            </div>
            <!-- /Content area -->
        </div>



    </div>

    @include('layout.component.footer')

    {{-- jquery click events --}}
    @include('layout.component.scripts')







</body>

</html>
