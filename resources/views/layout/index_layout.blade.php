<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap_limitness.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap_limitness.min.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/colours.min.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/colours.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/components.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/components.min.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/layout.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/layout.min.css') }}">



        <link type="text/css" rel="stylesheet" href="{{ URL::asset('global_assets/css/extras/animate.min.css') }}">


        <script src="{{ URL::asset('global_assets/js/main/jquery.min.js')}}"></script>
        <script src="{{URL::asset('global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
        <script src="{{URL::asset('global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
        <script src="{{URL::asset('global_assets/js/plugins/ui/ripple.min.js')}}"></script>







    </head>
</head>
<body>
    <div>
        @yield('content')
    </div>




</body>
</html>
