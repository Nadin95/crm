@extends('layout.admin_layout')


@section('content')


    <div class="container">
        <div class="content">

            <div class="row">
                {{-- <div class="col-lg-6"> --}}
                <div class="container">
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h6 class="card-title">Create Admin</h6>

                        </div>

                        <div class="error">

                        </div>

                        <div class="card-body">


                            <div class="row">

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" id="name" name="name" class="form-control"
                                            placeholder="Enter Name">
                                        <span class="text-danger" id="nameError"></span>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Mail</label>
                                        <input type="text" class="form-control" id="email" name="email"
                                            placeholder="Enter Mail">
                                        <span class="text-danger" id="emailError"></span>
                                    </div>
                                </div>



                            </div>


                            <label>Contact Number</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="tel" id="contact" name="contact" class="form-control"
                                            placeholder="Enter Contact NUmber">
                                        <span class="text-danger" id="contactError"></span>
                                    </div>
                                </div>


                            </div>


                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" id="password"
                                            placeholder="Enter Password">
                                        <span class="text-danger" id="passwordError"></span>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Re Enter Password</label>
                                        <input type="password" class="form-control" name="re_pass" id="re_pass"
                                            placeholder="Enter Password">
                                        <span class="text-danger" id="passwordError"></span>
                                    </div>
                                </div>


                            </div>




                            <div class="text-right">
                                <input type="hidden" id="edit_id" value="">
                                <button type="submit" class="btn btn-primary" onclick="admin.create()">Submit <i
                                        class="icon-paperplane ml-2"></i></button>
                            </div>

                        </div>
                    </div>


                </div>


            </div>



            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Admin Details</h5>

                </div>



                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Mail</th>
                                <th>Contact Number</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if (!empty($admin_data))

                                @foreach ($admin_data as $data)

                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->email }}</td>
                                        <td>{{ $data->contact_number }}</td>
                                        <th>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-secondary"
                                                    onclick="admin.edit({{ $data->id }})">Edit</button>
                                                <button type="button" class="btn btn-danger"
                                                    onclick="admin.delete({{ $data->id }})">Delete</button>
                                            </div>
                                        </th>

                                    </tr>

                                @endforeach

                            @endif



                        </tbody>

                    </table>

                    <span>
                        {{ $admin_data->links() }}
                    </span>

                    <style>
                        .w-5 {
                            display: none;
                        }

                    </style>
                </div>
            </div>




        @endsection
