@extends('layout.admin_layout')


@section('content')


<div class="container">
<div class="content">

    <!-- Default styling -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Admin Details</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        {{-- <div class="card-body">
            Example of a table with default styling. By default any table with <code>.table</code> class has <code>transparent</code> background color and grey border color. Table header and table footer have the same styling: transparent background, semibold font weight, darker horizontal border and the same height as table body cells.
        </div> --}}

        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Mail</th>
                        <th>Contact Number</th>
                    </tr>
                </thead>
                <tbody>

                 @if(!empty($admin_data))

                 @foreach ($admin_data as $data)

                 <tr>
                    <td>{{$data->id}}</td>
                    <td>{{$data->name}}</td>
                    <td>{{$data->email}}</td>
                    <td>{{$data->contact_number}}</td>

                </tr>

                 @endforeach

                 @endif



                </tbody>

            </table>
        </div>
    </div>
</div>


@endsection
