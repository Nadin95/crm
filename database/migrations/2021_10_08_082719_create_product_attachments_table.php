<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attachments', function (Blueprint $table) {
            $table->bigIncrements('id', true);
            $table->bigInteger('product_id')->nullable()->index('fk_product_images_product');
            $table->bigInteger('attachment_type_id')->nullable();
            $table->string('name', 180)->nullable();
            $table->string('title', 180)->nullable();
            $table->integer('order')->nullable()->default(99999);
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_attachments');
    }
}
