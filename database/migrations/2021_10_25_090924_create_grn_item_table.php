<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrnItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grn_item', function (Blueprint $table) {
            $table->id();

            $table->string('grn_id')->nullable();
            $table->string('product_variant')->nullable();
            $table->integer('quantity')->default(0);
            $table->decimal('unite_price',9,2)->default('0.00');
            $table->integer('is_processed')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grn_item');
    }
}
