<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variant', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            // $table->foreign('product_id')->references('product_id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $table->string('p_variant')->nullable();
            $table->double('width')->nullable();
            $table->double('height')->nullable();
            $table->double('length')->nullable();
            $table->double('depth')->nullable();
            $table->double('diameter')->nullable();
            $table->double('weight')->nullable();
            $table->integer('product_weight_unit_id')->nullable();
            $table->integer('product_dimension_unit_id')->nullable();
            $table->integer('volume')->nullable();
            $table->integer('product_volume_unit_id')->nullable();
            $table->double('rating')->default(0);
            $table->string('color')->nullable();
            $table->string('image')->nullable();
            $table->decimal('price',9,2)->default('0.00');
            $table->decimal('discount',9,2)->default('0.00');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variant');
    }
}
