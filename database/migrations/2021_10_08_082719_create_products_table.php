<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('product_id');


            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('category')->onUpdate('cascade')->onDelete('cascade');




            $table->unsignedBigInteger('subcategory_id')->nullable();
            $table->foreign('subcategory_id')->nullable()->references('id')->on('subcategory')->onUpdate('cascade')->onDelete('cascade');



            $table->unsignedBigInteger('sub_subcategory_id')->nullable();
            $table->foreign('sub_subcategory_id')->nullable()->references('id')->on('sub_subcategory')->onUpdate('cascade')->onDelete('cascade');

            $table->string('title', 180)->nullable();
            $table->string('product_code', 60)->nullable();
            $table->string('name');
            $table->mediumText('description');

            $table->text('product_special_notice')->nullable();
            $table->string('unit', 60)->nullable();
            $table->integer('is_featured')->nullable();
            $table->integer('is_toprated')->nullable();

            $table->integer('product_approve_by')->nullable();
            $table->dateTime('product_approve_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
