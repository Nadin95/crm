<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grn', function (Blueprint $table) {
            $table->id();
            $table->string('grn_number')->nullable();
            $table->string('grn_title')->nullable();
            $table->string('grn_description')->nullable();
            $table->string('grn_created_by')->nullable();
            $table->integer('is_approved')->default(0);
            $table->string('grn_approved_by')->nullable();
            $table->string('quentity')->nullable();
            $table->string('supplier')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grn');
    }
}
