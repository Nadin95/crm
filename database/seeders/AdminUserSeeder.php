<?php

namespace Database\Seeders;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use DB;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name'=>'Admin',
            'email'=>'admin@gmail.com',
            'password' => Hash::make('123456'),
            'contact_number'=>'0774848449'
        ]);

        DB::table('product_dimension_units')->insert([
            'dimension_unit_name'=>'D_name',
            'dimension_unit_symbol'=>'FF',
        ]);

        DB::table('product_volume_units')->insert([
            'volume_unit_name'=>'vname',
            'volume_unit_sign'=>'vsign'
        ]);
        DB::table('product_weight_units')->insert([
            'weight_unit_name'=>'w_name',
            'weight_unit_sign'=>'e_u_s'
        ]);





    }
}
