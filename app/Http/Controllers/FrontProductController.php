<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductVariant;

class FrontProductController extends Controller
{
    public function showproduct($id)
    {
        $product_details=Product::where('deleted_at',null)->where('product_id',$id)->first();

        $product_variant=ProductVariant::where('deleted_at',null)->where('product_id',$id)->first();
        $product_variants=ProductVariant::where('deleted_at',null)->where('product_id',$id)->first();


        return view('frontend.layout.pages.product')->with([
            'product_details'=>$product_details,
            'product_variant'=>$product_variant,
            'product_variants'=>$product_variants
        ]);

    }
}
