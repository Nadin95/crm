<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Sub_SubCategory;
use App\Models\Product;
use App\Models\Product_Weight;
use App\Models\Product_Volume;
use App\Models\Product_Dimention;
use App\Models\ProductImage;
use App\Models\ProductVariant;
use App\Models\ProductPrice;
use Illuminate\Support\Facades\Session;

use App\Models\Admin;

class ProductController extends Controller
{

    public function createproductview()
    {

        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

        $categories=Category::where('deleted_at',null)->get();
        $subcategories=SubCategory::where('deleted_at',null)->get();
        $sub_sub_categories=Sub_SubCategory::where('deleted_at',null)->get();
        $product_weight=Product_Weight::where('deleted_at',null)->get();
        $product_volume=Product_Volume::where('deleted_at',null)->get();
        $product_dimention=Product_Dimention::where('deleted_at',null)->get();
        $product=Product::where('deleted_at',null)->latest('product_id')->first();
        // dd($product);





        // dd($product_weight);

        return view('products/addproduct')->with([
            'categories'=>$categories,
            'subcategories'=>$subcategories,
            'sub_sub_categories'=>$sub_sub_categories,
            'product_weight'=>$product_weight,
            'product_volume'=>$product_volume,
            'product_dimention'=>$product_dimention,
            'product'=>$product,
            'user_details'=>$user_details
        ]);

    }


    public function viewvariant()
    {

        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

        $product_weight=Product_Weight::where('deleted_at',null)->get();
        $product_volume=Product_Volume::where('deleted_at',null)->get();
        $product_dimention=Product_Dimention::where('deleted_at',null)->get();


        return view('products/addproduct-variant')->with([
            'product_weight'=>$product_weight,
            'product_volume'=>$product_volume,
            'product_dimention'=>$product_dimention,
            'user_details'=>$user_details
        ]);

    }

   public function createproduct(Request $req)
   {



     $cat_id=$req->cat_id;
     $sub_cat_id=$req->sub_cat_id;
     $subsub_cat_id=$req->subsub_cat_id;
     $title=$req->title;
     $p_code=$req->p_code;
     $name=$req->name;
     $des=$req->des;
     $width=$req->width;
     $height=$req->height;
     $length=$req->length;
     $depth=$req->depth;
     $diameter=$req->diameter;
     $weight=$req->weight;
     $volume=$req->volume;
     $image_array=$req->file('files');
     $product_id=$req->product_id;


    $variant_width=$req->variant_width;
    $variant_height=$req->variant_height;
    $variant_length=$req->variant_length;
    $variant_depth=$req->variant_depth;
    $variant_diameter=$req->variant_diameter;
    $variant_weight=$req->variant_weight;
    $variant_volume=$req->variant_volume;
    $variant_price=$req->variant_price;
    $variant_w_unit=$req->variant_w_unit;
    $variant_v_unit=$req->variant_v_unit;
    $variant_d_unit=$req->variant_d_unit;
    $variant_discount=$req->variant_discount;
    // dd($variant_v_unit,$variant_d_unit,$variant_w_unit);
    // dd($variant_discount);
    $variant_color=$req->variant_color;
    // dd($variant_color);

        $data=$req->validate([
            'cat_id'=>'required',
            'sub_cat_id'=>'required',
            'subsub_cat_id'=>'required',
            'title'=>'required',
            'p_code'=>'required',
            'name'=>'required',
            'des'=>'required',
            'variant_width.*'=>'required',
            'variant_height.*'=>'required',
            'variant_length.*'=>'required',
            'variant_depth.*'=>'required',
            'variant_diameter.*'=>'required',
            'variant_weight.*'=>'required',
            'variant_volume.*'=>'required',
            'variant_price.*'=>'required',
            'product_id'=>'required',
            'files' => 'required',
            'files.*' => 'mimes:jpeg,jpg,png,gif,csv,txt,pdf|max:2048',
            'variant_w_unit.*'=>'required',
            'variant_v_unit.*'=>'required',
            'variant_d_unit.*'=>'required',
            'variant_discount.*'=>'required',
            'variant_color.*'=>'required',

        ]);
        $new_product=Product::create([
                        'category_id'=>$data['cat_id'],
                        'subcategory_id'=>$data['sub_cat_id'],
                        'sub_subcategory_id'=>$data['subsub_cat_id'],
                        'title'=>$data['title'],
                        'product_code'=>$data['p_code'],
                        'name'=>$data['name'],
                        'description'=>$data['des']


        ]);

        // $this->addproduct_cat($product_id);






        $this->savevariants($product_id,$variant_discount,$variant_width,$variant_height,$variant_length,$variant_depth,
        $variant_diameter,$variant_weight,$variant_volume,$variant_price,$variant_d_unit,$variant_v_unit,$variant_w_unit,$variant_color);
        $this->uploadproductImage($image_array,$product_id);

        return back()->with('success', 'Data has successfully Saved!');
   }

   public function savevariants($product_id,$variant_discount,$variant_width,$variant_height,$variant_length,$variant_depth,$variant_diameter,$variant_weight,$variant_volume,$variant_price,$variant_d_unit,$variant_v_unit,$variant_w_unit,$variant_color)
   {

    // dd($variant_color);

    foreach($variant_width as $key=>$value)
    {

        $save_varients=ProductVariant::create([

        'product_id'=>$product_id,
        'width'=>$variant_width[$key],
        'height'=>$variant_height[$key],
        'length'=>$variant_length[$key],
        'depth'=>$variant_depth[$key],
        'diameter'=>$variant_diameter[$key],
        'product_dimension_unit_id'=>$variant_d_unit[$key],
        'product_weight_unit_id'=>$variant_w_unit[$key],
        'weight'=>$variant_weight[$key],
        'volume'=>$variant_volume[$key],
        'product_volume_unit_id'=>$variant_v_unit[$key],
        'price'=>$variant_price[$key],
        'discount'=>$variant_discount[$key],
        'color'=>$variant_color[$key]



    ]);

    }


   }

   public function uploadproductImage($image_array,$product_id)
   {


        if($image_array)
        {
                foreach($image_array as $image)
                {
                            $Img_name='pro_'.(bin2hex(random_bytes('6')));
                            $duplicateValue=ProductImage::select('id as Identification','name as name')->where('name',$Img_name)->first();
                            // $product_id=$req->product_id;
                            $folder_path='/products/';

                            //product name
                            $product_id=$product_id;

                            $product=Product::where('product_id',$product_id)->where('deleted_at',null)->first();
                            $product_name=$product->name;



                                                if(empty($duplicateValue))
                                                {
                                                    $path=public_path().$folder_path;
                                                    if(!(ProductImage::exists($path)))
                                                    {
                                                        \File::makeDirectory($path,$mode=0775,true,true);
                                                    }

                                                        $extention=$image->getClientOriginalExtension();

                                                        $file_name=$Img_name.'.'.$extention;

                                                        $image->move($path,$file_name);
                                                        $url=$folder_path.$file_name;

                                                            $new_image=ProductImage::create([
                                                                    'name'=>$product_name,
                                                                    'image'=>$url,
                                                                    'product_id'=>$product_id,

                                                                    ]);

                                                }
                }

        }
    }


// ///////////////////////////////////////////////////virw product

public function viewproductlist()
{

    $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();


    $products=Product::where('deleted_at',null)->get();
    // $product_variant=ProductVariant::where('deleted_at',null)->get();

    return view('products.productlist')->with([
        'products'=>$products,
        'user_details'=>$user_details

    ]);

}



public function editview($id)
{
    // $id=$req->id;
    $user_id=Session::get('admin');
    $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();


    $product=Product::where('deleted_at',null)->where('product_id',$id)->first();
    $product_variants=ProductVariant::where('deleted_at',null)->where('product_id',$id)->get();
    $categories=Category::where('deleted_at',null)->get();
    $subcategories=SubCategory::where('deleted_at',null)->where('parent_id', $product->category_id)->get();
    $sub_sub_categories=Sub_SubCategory::where('deleted_at',null)->where('parent_id', $product->subcategory_id)->get();
    $product_weight=Product_Weight::where('deleted_at',null)->get();
    $product_volume=Product_Volume::where('deleted_at',null)->get();
    $product_dimention=Product_Dimention::where('deleted_at',null)->get();
    $product_image=ProductImage::where('deleted_at',null)->where('product_id',$id)->get();




    return view('products.editproduct')->with([

        'product'=>$product,
        'product_variants'=>$product_variants,
        'categories'=>$categories,
        'subcategories'=>$subcategories,
        'sub_sub_categories'=>$sub_sub_categories,
        'product_weight'=>$product_weight,
        'product_volume'=>$product_volume,
        'product_dimention'=>$product_dimention,
        'product_image'=>$product_image,
        'user_details'=>$user_details
    ]);

    return response()->json('success');


}


public function updateproduct(Request $req)


{

     $cat_id=$req->cat_id;
     $sub_cat_id=$req->sub_cat_id;
     $subsub_cat_id=$req->subsub_cat_id;
     $title=$req->title;
     $p_code=$req->p_code;
     $name=$req->name;
     $des=$req->des;
     $width=$req->width;
     $height=$req->height;
     $length=$req->length;
     $depth=$req->depth;
     $diameter=$req->diameter;
     $weight=$req->weight;
     $volume=$req->volume;
     $image_array=$req->file('files');
     $product_id=$req->product_id;


    $variant_width=$req->variant_width;
    $variant_height=$req->variant_height;
    $variant_length=$req->variant_length;
    $variant_depth=$req->variant_depth;
    $variant_diameter=$req->variant_diameter;
    $variant_weight=$req->variant_weight;
    $variant_volume=$req->variant_volume;
    $variant_price=$req->variant_price;
    $variant_w_unit=$req->variant_w_unit;
    $variant_v_unit=$req->variant_v_unit;
    $variant_d_unit=$req->variant_d_unit;
    $variant_discount=$req->variant_discount;


    $data=$req->validate([
        'cat_id'=>'required',
        'sub_cat_id'=>'required',
        'subsub_cat_id'=>'required',
        'title'=>'required',
        'p_code'=>'required',
        'name'=>'required',
        'des'=>'required',
        'variant_width.*'=>'required',
        'variant_height.*'=>'required',
        'variant_length.*'=>'required',
        'variant_depth.*'=>'required',
        'variant_diameter.*'=>'required',
        'variant_weight.*'=>'required',
        'variant_volume.*'=>'required',
        'variant_price.*'=>'required',
        'product_id'=>'required',
        'files' => 'required',
        'files.*' => 'mimes:jpeg,jpg,png,gif,csv,txt,pdf|max:2048',
        'variant_w_unit.*'=>'required',
        'variant_v_unit.*'=>'required',
        'variant_d_unit.*'=>'required',
        'variant_discount.*'=>'required'


    ]);

    $update_product=Product::where('product_id',$product_id)->update([
        'category_id'=>$cat_id,
        'subcategory_id'=>$sub_cat_id,
        'sub_subcategory_id'=>$subsub_cat_id,
        'title'=>$title,
        'product_code'=>$p_code,
        'name'=>$name,
        'description'=>$des
    ]);


    $this->updateproductImage($image_array,$product_id);

    return back()->with('success', 'Data has successfully Saved!');





}

public function updateproductImage($image_array,$product_id)
   {


        if($image_array)
        {
                foreach($image_array as $image)
                {
                            $Img_name='pro_'.(bin2hex(random_bytes('6')));
                            $duplicateValue=ProductImage::select('id as Identification','name as name')->where('name',$Img_name)->first();
                            // $product_id=$req->product_id;
                            $folder_path='/products/';

                            $product_id=$product_id;

                                                if(empty($duplicateValue))
                                                {
                                                    $path=public_path().$folder_path;
                                                    if(!(ProductImage::exists($path)))
                                                    {
                                                        \File::makeDirectory($path,$mode=0775,true,true);
                                                    }

                                                        $extention=$image->getClientOriginalExtension();

                                                        $file_name=$Img_name.'.'.$extention;

                                                        $image->move($path,$file_name);
                                                        $url=$folder_path.$file_name;
                                                        // dd($url);

                                                            $update_image=ProductImage::where('product_id',$product_id)->update([
                                                                'name'=>$Img_name,
                                                                'image'=>$url,
                                                                'product_id'=>$product_id
                                                            ]);
                                                            // $update_image->name=$Img_name;
                                                            // $update_image->image=$url;
                                                            // $update_image->product_id=$product_id;
                                                            // $update_image->save();




                                                }
                }

        }
    }



   public function vieweditvariant($id)
   {
    $user_id=Session::get('admin');
    $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

    $variant=ProductVariant::where('id',$id)->where('deleted_at',null)->get();
    $product_weight=Product_Weight::where('deleted_at',null)->get();
    $product_volume=Product_Volume::where('deleted_at',null)->get();
    $product_dimention=Product_Dimention::where('deleted_at',null)->get();
    $variant_id=ProductVariant::where('id',$id)->where('deleted_at',null)->first();

    // dd($variant_id);

    return view('products.editvariant')->with([
        'variant'=>$variant,
        'product_weight'=>$product_weight,
        'product_volume'=>$product_volume,
        'product_dimention'=>$product_dimention,
        'variant_id'=>$variant_id,
        'user_details'=>$user_details
    ]



    );


   }

   public function updatevariant(Request $req)
   {

    $v_id=$req->variant_id;
    $width=$req->width;
    $height=$req->height;
    $length=$req->length;
    $depth=$req->depth;
    $weight=$req->weight;
    $volume=$req->volume;
    $price=$req->price;
    $w_unit=$req->w_unit;
    $v_unit=$req->v_unit;
    $d_unit=$req->d_unit;
    $diameter=$req->diameter;
    $discount=$req->discount;
    // dd($discount);

    // dd($v_id);

    $data =$req->validate([
        'width'=>'required',
        'height'=>'required',
        'length'=>'required',
        'depth'=>'required',
        'diameter'=>'required',
        'weight'=>'required',
        'volume'=>'required',
        'price'=>'required',
        'discount'=>'required',
        'w_unit'=>'required',
        'v_unit'=>'required',
        'd_unit'=>'required',


    ]);

    ProductVariant::where('id',$v_id)->where('deleted_at',null)->update([
        'width'=>$width,
        'height'=>$height,
        'length'=>$length,
        'depth'=>$depth,
        'diameter'=>$diameter,
        'weight'=>$weight,
        'volume'=>$volume,
        'price'=>$price,
        'discount'=>$discount,
        'product_weight_unit_id'=>$w_unit,
        'product_volume_unit_id'=>$v_unit,
        'product_dimension_unit_id'=>$d_unit
    ]);


    return back()->with('success', 'Data has successfully Saved!');



   }

   public function removeimage($id)
   {


    ProductImage::find($id)->delete();

    return response()->json('success');

   }

//    public function addproduct_cat($product_id)
//    {
//     $record=Product::first($product_id);
//     $record->products_cat()->attach();


//    }







}
