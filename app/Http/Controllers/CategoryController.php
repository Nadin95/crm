<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Sub_SubCategory;
use App\Models\Admin;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Session;
use File;

class CategoryController extends Controller
{
    public function ViewCategories()
    {

        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();
      $category=Category::where('deleted_at',null)->get();
      $subcategory=SubCategory::where('deleted_at',null)->where('parent_id')->with('categoey')->get();
      $sub_subcatergory=Sub_SubCategory::where('deleted_at',null)->with('sub_category')->get();

      return view('category/category_list')
                     ->with([
                        'category'=>$category,
                        'subcategory'=>$subcategory,
                        'sub_subcatergory'=>$sub_subcatergory,
                        'user_details'=>$user_details

                        ]);


    }
    public function ViewCategory($id)
    {
        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();
        $category_details=Category::where('deleted_at',null)->where('id',$id)->get();

            return view('category/categoryview')
                    ->with([
                        'category_details'=>$category_details,
                        'user_details'=>$user_details
                    ]);


    }

    public function ViewAddCategory()
    {
        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

        $category=Category::where('deleted_at',null)->get();
        $subcategories=SubCategory::where('deleted_at',null)->get();
            return view('category/create-category')
            ->with([
                    'category'=>$category,
                    'subcategories'=>$subcategories,
                    'user_details'=>$user_details,
            ]);

    }

    public function AddCategory(Request $req)
    {


        $category_name=$req->categoey_name;
        $category_description=$req->category_description;
        $folder_path='/category/';


                $data=$req->validate([
                        'categoey_name'=>'required',
                        'category_description'=>'required',
                ]);

        $Img_name=(bin2hex(random_bytes('6')));
        $duplicateValue=Category::select('id as Identification','name as name')->where('name',$Img_name)->first();


        if(empty($duplicateValue))
        {
            if($req->hasFile('files'))
            {
                $path=public_path().$folder_path;
                if(!(Category::exists($path)))
                {
                    \File::makeDirectory($path,$mode=0775,true,true);
                }
                foreach ($req->file('files') as $file)
                {
                    $extention=$file->getClientOriginalExtension();
                    $file_name=$Img_name.'.'.$extention;

                    // dd($file_name);

                    $file->move($path,$file_name);
                    $url=$folder_path.$file_name;


                    $this->Image($url);


                    $new_category=Category::create([

                        'name'=>$data['categoey_name'],
                        'description'=>$data['category_description'],
                        'image'=>$url,

                    ]);
                }

            }
        }


        return back()->with('success', 'Data has successfully Saved!');
    }



    public function ViewCategoryUpdate($id)
    {

        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

        $category_detail=Category::where('deleted_at',null)->where('id',$id)->first();
        $category_details=Category::where('deleted_at',null)->where('id',$id)->get();


        return view('category/categoryupdate')->with([
            'category_detail'=>$category_detail,
            'category_details'=>$category_details,
            'user_details'=>$user_details
        ]);

    }

    public function CategoryUpdate(Request $req)
    {
        $name=$req->category_name;
        $description=$req->category_description;
        $id=$req->h_id;
        $folder_path='/category/';


        $Img_name=(bin2hex(random_bytes('6')));

            if($req->hasFile('files'))
            {
                $path=public_path().$folder_path;



                    if(!(Category::exists($path)))
                    {
                        \File::makeDirectory($path,$mode=0775,true,true);
                    }

                        foreach ($req->file('files') as $file)
                        {
                            $extention=$file->getClientOriginalExtension();
                            $file_name=$Img_name.'.'.$extention;

                            $file->move($path,$file_name);
                            $url= $folder_path.$file_name;




                                $update_category=Category::find($id);
                                $update_category->name=$name;
                                $update_category->description=$description;
                                $update_category->image=$url;
                                $update_category->save();
                        }
            }else{
                $update_category=Category::find($id);
                $update_category->name=$name;
                $update_category->description=$description;
                $update_category->save();
            }

            return back()->with('success', 'Data has successfully Saved!');
    }

    public function CategoryDelete(Request $req)
    {
        $id=$req->id;
        Category::find($id)->delete();
        return response()->json('success');

    }


    public function ViewSubCategoey($id)
    {


        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();


        $sub_category_details=SubCategory::where('deleted_at',null)->where('id',$id)->get();

        return view('category/subcategory/subcategory')->with([
            'sub_category_details'=>$sub_category_details,
            'user_details'=>$user_details,


        ]);
    }



    public function AddSubCategory(Request $req)
    {
        $category_name=$req->categoey_name;
        $category_description=$req->category_description;
        $folder_path='/category/subcategory/';

        $cat_id=$req->category_id;


        $data=$req->validate([
            'category_id'=>'required',
            'categoey_name'=>'required',
            'category_description'=>'required',
        ]);

        $Img_name=(bin2hex(random_bytes('6')));
        $duplicateValue=Category::select('id as Identification','name as name')->where('name',$Img_name)->first();

        if(empty($duplicateValue))
        {

            if($req->hasFile('files'))
            {
                $path=public_path().$folder_path.'/';

                if(!(Category::exists($path)))
                {
                    \File::makeDirectory($path,$mode=0775,true,true);
                }

                    foreach ($req->file('files') as $file)
                    {

                        $extention=$file->getClientOriginalExtension();
                        $file_name=$Img_name.'.'.$extention;


                        $file->move($path,$file_name);
                        $url=$folder_path.$file_name;

                        $this->Image($url);

                            $new_category=SubCategory::create([

                                        'parent_id'=>$data['category_id'],
                                        'name'=>$data['categoey_name'],
                                        'description'=>$data['category_description'],
                                        'image'=>$url,

                                                            ]);


                    }

            }
        }
        return back()->with('success', 'Data has successfully Saved!');

    }



    public function ViewSubCategoryUpdate($id)
    {
        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

        $sub_category_details=SubCategory::where('deleted_at',null)->where('id',$id)->first();
        $category_details=Category::where('deleted_at',null)->where('id',$id)->get();
        // $product=Product::where('deleted_at',null)->latest('product_id')->first();
        return view('category/subcategory/editsubcategory')->with([
            'sub_category_details'=>$sub_category_details,
            'category_details'=>$category_details,
            'user_details'=>$user_details
        ]);

    }

    public function SubCategoryUpdate(Request $req)
    {


        $name=$req->sub_category_name;
        $description=$req->sub_category_description;
        $category_id=$req->category_id;
        $id=$req->h_id;
        // dd($category_id);

        $folder_path='/category/subcategory/';


        $Img_name=(bin2hex(random_bytes('6')));



            if($req->hasFile('files'))
            {
                $path=public_path().$folder_path;

                    if(!(Category::exists($path)))
                    {
                        \File::makeDirectory($path,$mode=0775,true,true);
                    }

                        foreach ($req->file('files') as $file)
                        {
                            $extention=$file->getClientOriginalExtension();
                            $file_name=$Img_name.'.'.$extention;

                            // dd($file_name);

                            $file->move($path,$file_name);
                            $url=$folder_path.$file_name;
                            // dd($url);
                            $this->Image($url);

                            $img = Image::make($url)->crop(100,100);
                            // $img->resize(100,100);
                            $img->save($url);


                                    $update_category=SubCategory::find($id);
                                    $update_category->parent_id=$category_id;
                                    $update_category->name=$name;
                                    $update_category->description=$description;
                                    $update_category->image=$url;
                                    $update_category->save();


                        }

            }else{

                $update_category=SubCategory::find($id);
                $update_category->parent_id=$category_id;
                $update_category->name=$name;
                $update_category->description=$description;
                $update_category->save();

            }


            return back()->with('success', 'Data has successfully Saved!');

    }

    public function SubCategoryDelete(Request $req)
    {
        $id=$req->id;
        SubCategory::find($id)->delete();

        return response()->json('success');


    }
    public function view_subsubCategory($id)
    {
        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

        $sub_subcategory_details=Sub_SubCategory::where('deleted_at',null)->where('id',$id)->first();
        $sub_subcatdetails=Sub_SubCategory::where('deleted_at',null)->where('id',$id)->get();
        return view('category/subcategory/sub_subcategory/sub_subcategory')->with([
            'sub_subcatdetails'=>$sub_subcatdetails,
            'sub_subcategory_details'=>$sub_subcategory_details,
            'user_details'=>$user_details
        ]);

    }


    public function Add_SubSub_Category(Request $req)
    {

        $name=$req->ssc_name;
        $description=$req->ssc_description;
        $sc_id=$req->sc_id;



        $folder_path='/category/subcategory/subsubcategory/';



        $Img_name=(bin2hex(random_bytes('6')));
        $duplicateValue=Category::select('id as Identification','name as name')->where('name',$Img_name)->first();

        if(empty($duplicateValue))
        {

            if($req->hasFile('files'))
            {
                $path=public_path().$folder_path;

                if(!(Category::exists($path)))
                {
                    \File::makeDirectory($path,$mode=0775,true,true);
                }

                    foreach ($req->file('files') as $file)
                    {
                        $extention=$file->getClientOriginalExtension();
                        $file_name=$Img_name.'.'.$extention;

                        $file->move($path,$file_name);
                        $url=$folder_path.$file_name;
                        // dd($url);

                      $this->Image($url);

                        $update_category=Sub_SubCategory::create([
                            'parent_id'=>$sc_id,
                            'name'=>$name,
                            'description'=>$description,
                            'image'=>$url,
                                                                ]);

                    }

            }
        }

        return back()->with('success', 'Data has successfully Saved!');


    }

    public function View_Edit_SubSub_Category($id)
    {

        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

        $sub_subcategory_details=Sub_SubCategory::where('deleted_at',null)->where('id',$id)->first();

        $sub_category_details=SubCategory::where('deleted_at',null)->get();

        // dd($sub_subcategory_details);

        return view('category/subcategory/sub_subcategory/edit_subsub_category')->with([
            'sub_subcategory_details'=>$sub_subcategory_details,
            'sub_category_details'=>$sub_category_details,
            'user_details'=>$user_details

        ]);

    }

    public function Edit_subsub_Category(Request $req)
    {
        $name=$req->ssc_name;
        $description=$req->ssc_dis;
        $id=$req->h_id;
        $parentcat_id=$req->sub_category_id;

        $Img_name=(bin2hex(random_bytes('6')));

        // dd($Img_name);

        $folder_path='/category/subcategory/subsubcategory/';



            if($req->hasFile('files'))
            {
                $path=public_path().$folder_path;
                // dd($path);

                if(!(Category::exists($path)))
                {
                    \File::makeDirectory($path,$mode=0775,true,true);
                }

                foreach ($req->file('files') as $file)
                {
                    $extention=$file->getClientOriginalExtension();
                    $file_name=$Img_name.'.'.$extention;

                    $file->move($path,$file_name);
                    $url=$folder_path.$file_name;


                    $this->Image($url);


                    $update_category=Sub_SubCategory::findOrFail($id);
                    // dd($update_category);
                    $update_category->parent_id=$parentcat_id;
                    $update_category->name=$name;
                    $update_category->description=$description;
                    $update_category->image=$url;
                    $update_category->save();




                }

            }else{
                $update_category=Sub_SubCategory::findOrFail($id);
                $update_category->parent_id=$parentcat_id;
                $update_category->name=$name;
                $update_category->description=$description;
                $update_category->save();


            }




            return back()->with('success', 'Data has successfully Saved!');
    }

    public function Delete_SubSub_Category(Request $req)
    {
        $id=$req->id;
        Sub_SubCategory::find($id)->delete();

        return response()->json('success');

    }

    public function Image($url)
    {
        // $img = Image::make($url)->resize(100,100);
        // $img->resize(100,100);
        // crop
        // $img->save($url);



    }









}
