<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

use App\Models\Product;
use App\Models\ProductImage;

class FrontCategoryController extends Controller
{

    public function showcategoeies()
    {
        $categories=Category::where('deleted_at',null)
        ->with('products','subcategories','subsubCategories')
        ->get();
        $products=Product::where('deleted_at',null)->with('Images')->get();
        // dd($categories);
        $product_images=ProductImage::where('deleted_at',null)->get();

        return view('frontend.layout.pages.category')->with([
            'categories'=>$categories,
            'products'=>$products,
            'product_images'=>$product_images

        ]);

    }
}
