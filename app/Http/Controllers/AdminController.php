<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class AdminController extends Controller
{
    public function ViewAdmin()
    {
        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

        return view('dashboard/index')->with([
            'user_details'=>$user_details
        ]);
    }


    public function ViewAdminAdd()
    {
        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();
        // dd($user);
        $admin_data=Admin::where('deleted_at',null)->paginate(10);
        return view('admin/create')->with([
            'admin_data'=>$admin_data,
            'user_details'=>$user_details

        ]);
    }


    public function CreateAdmin(Request $req)
    {



        $data=$this->validate($req, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'contact_number' => 'required',
        ]);

        $edit_data=Admin::where('id',$req->h_id)->first();

        if($edit_data===null){
            $new_admin=Admin::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'contact_number' => $data['contact_number'],
            ]);

            return response()->json('success');
        }else{
            Admin::where('id',$req->h_id)->update([
                'name'=>$data['name'],
                'email'=>$data['email'],
                'password'=>Hash::make($data['password']),
                'contact_number'=>$data['contact_number']
            ]);
            return response()->json('Updated');


        }






    }

    // public function ViewAdminList()
    // {

    //     $admin_data=Admin::where('deleted_at',null)->get();

    //     return view('admin/create')->with([
    //         'admin_data'=>$admin_data
    //     ]);


    // }

    public function DeleteAdmin($id)
    {
        Admin::find($id)->delete();

        return response()->json('success');


    }

    public function EditAdmin(Request $req)
    {
        $id=$req->id;
        $edit_data=Admin::where('id',$id)->get();

        return response()->json($edit_data);




    }


}
