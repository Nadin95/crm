<?php

namespace App\Http\Controllers;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminLoginController extends Controller
{
    public function showLogin()
    {
        return view('login/adminlogin');

    }
    public function check_admin(Request $request)
    {

        $email = $request->email;
        $pass = $request->password;


        $data = Admin::where('email', $email)->first();

        // Auth::attempt(['email'=>$email,'password'=>$password])
        // dd(!empty($data) && Hash::check($pass,$data->password))

        if(!empty($data) && Hash::check($pass,$data->password)){
            $request->session()->put('admin', $data->id);
            return redirect('/admin/index');
        }else{
            return redirect('/admin/login?error=true');
        }
    }


    public function Signout(){

        Session::flush();
        return redirect('/admin/login');

    }








}
