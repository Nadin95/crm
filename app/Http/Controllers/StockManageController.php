<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\Admin;
use App\Models\GRN;
use App\Models\Grnvariant;

class StockManageController extends Controller
{
    public function creategrnview()
    {
        $user_id=Session::get('admin');
        $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();



        return view('grn/create_grn')->with([
            'user_details'=>$user_details,
        ]);

    }
   public function creategrn(Request $req)
   {

    $grn_number=$req->grn_number;
    $grn_title=$req->grn_title;
    $grn_description=$req->grn_description;
    $approved=$req->approved;
    $quentity=$req->quentity;
    $supplier=$req->supplier;

    if($req->has('approved_status')){
        //Checkbox checked
        $approved=1;
    }else{
        //Checkbox not checked
        $approved=0;
    }


    $user_id=Session::get('admin');
    $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

    $GRN_code='grn_'.(bin2hex(random_bytes('6')));
    $duplicateValue=GRN::where('grn_number',$GRN_code)->first();







    $data=$this->validate($req, [
        'grn_title' => 'required',
        'grn_description' => 'required',
        'quentity' => 'required',
        'supplier' => 'required',
    ]);


    if(empty($duplicateValue))
    {

        $creategrn=GRN::create([
            'grn_number'=>$GRN_code,
            'grn_title'=>$grn_title,
            'grn_description'=>$grn_description,
            'grn_created_by'=>$user_details->name,
            'is_approved'=>$approved,
            'grn_approved_by'=>$user_details->name,
            'quentity'=>$quentity,
            'supplier'=>$supplier,
        ]);

        $record_id=$creategrn->id;

    }
    // return back()->with('success', 'Data has successfully Saved!');
    // return redirect()->route('edit/'.$record_id);

    return response()->json($record_id);
    //

   }



   public function editgrnview($id)
   {
    $user_id=Session::get('admin');
    $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

    $grn_details=GRN::where('id',$id)->where('deleted_at',null)->first();


    // GRN


    return view('grn/editgrn_variant')->with([
        'user_details'=>$user_details,
        'grn_details'=>$grn_details
    ]);



   }

   public function updategrnVatiants(Request $req)
   {

    $code=$req->code;
    $variant=$req->variant;
    $quantity=$req->quantity;
    $unit_price=$req->unit_price;
    $processed=$req->processed;



    $data=$this->validate($req, [

        'variant' => 'required',
        'quantity' => 'required',
        'unit_price' => 'required',
        // 'processed' => 'required',
    ]);

    $add_stock_variant=Grnvariant::create([
        'grn_id'=>$code,
        'product_variant'=>$variant,
        'quantity'=>$quantity,
        'unite_price'=>$unit_price,
        'is_processed'=>$processed,
    ]);

    return response()->json('success');

   }

   public function viewgrn()
   {
    $user_id=Session::get('admin');
    $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();
    $grn_details=GRN::where('deleted_at',null)->get();
    $grn_vatiants=Grnvariant::where('deleted_at',null)->get();

    return view('grn.viewgrn')->with([
        'user_details'=>$user_details,
        'grn_details'=>$grn_details,
        'grn_vatiants'=>$grn_vatiants
    ]);

   }

   public function viewupdategrn($id)
   {
       dd($id);
    $user_id=Session::get('admin');
    $user_details=Admin::where('deleted_at',null)->where('id',$user_id)->first();

    $grn_details=GRN::where('deleted_at',null)->where('id',$id)->get();
    $grn_detail=GRN::where('deleted_at',null)->where('id',$id)->first();
    $grn_code=$grn_detail->grn_number;
    $grn_vatiants=Grnvariant::where('deleted_at',null)->where('grn_id',$grn_code)->get();


    return view('grn.updategrn')->with([
        'user_details'=>$user_details,
        'grn_details'=>$grn_details,
        'grn_vatiants'=>$grn_vatiants,
        'grn_detail'=>$grn_detail
    ]);


   }


}
