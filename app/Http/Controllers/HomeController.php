<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class HomeController extends Controller
{
    public function showhome()
    {
        $categories=Category::where('deleted_at',null)->take(4)->get();

        return view('frontend.layout.pages.index')->with([
            'categories'=>$categories
        ]);
    }
}
