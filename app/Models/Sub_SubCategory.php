<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sub_SubCategory extends Model
{
    use HasFactory;

    protected $table='sub_subcategory';
    use SoftDeletes;
    protected $dates=['deleted_at'];

    protected $fillable=['parent_id','name','description','image'];





    public function sub_category()
    {
        return $this->belongsTo(SubCategory::class,'parent_id','id');

    }
}
