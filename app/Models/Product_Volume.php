<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Volume extends Model
{
    use HasFactory;


    protected $table='product_volume_units';

    use SoftDeletes;
    protected $dates=['deleted_at'];

    protected $fillable=[
        'volume_unit_id',
        'volume_unit_name',
        'volume_unit_sign'
    ];


}
