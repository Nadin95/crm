<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
    use HasFactory;

    protected $table='subcategory';
    use SoftDeletes;
    protected $dates=['deleted_at'];

    protected $fillable=['parent_id','name','description','image'];

    public function category()
    {
        return $this->belongsTo(Category::class,'parent_id','id');
    }
    public function Su_subbCategoey()
    {
        return $this->hasMany(Sub_SubCategory::class,'parent_id','id');
    }
}
