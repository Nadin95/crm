<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Weight extends Model
{
    use HasFactory;

    protected $table='product_weight_units';

    use SoftDeletes;
    protected $dates=['deleted_at'];



    protected $fillable=[
        'weight_unit_id','weight_unit_name','weight_unit_sign'
    ];
}
