<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product_Dimention extends Model
{

    use HasFactory;

    use SoftDeletes;
    protected $dates=['deleted_at'];

    protected $table='product_dimension_units';
    protected $fillable=['dimension_unit_id','dimension_unit_name','dimension_unit_symbol'];
}
