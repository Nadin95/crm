<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVariant extends Model
{
    use HasFactory;

    use SoftDeletes;
    protected $dates=['deleted_at'];


    protected $table='product_variant';

    protected $fillable=[
        'product_id',
        'width',
        'height',
        'length',
        'depth',
        'diameter',
        'weight',
        'product_dimension_unit_id',
        'product_weight_unit_id',
        'volume',
        'product_volume_unit_id',
        'color',
        'image',
        'price',
        'discount',



    ];

}
