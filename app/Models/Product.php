<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Product extends Model
{
    use HasFactory;

    use SoftDeletes;
    protected $dates=['deleted_at'];

    protected $table='products';

    protected $primaryKey = 'product_id';

    protected $fillable=[

        'product_id',
        'category_id',
        'subcategory_id',
        'sub_subcategory_id',
        'title',
        'product_code',
        'name',
        'description',
        'product_special_notice',
        'unit',
        'discount'
                       ];




        public function Images()
        {
            return $this->hasMany(ProductImage::class,'product_id','product_id');
        }



}
