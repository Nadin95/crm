<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GRN extends Model
{
    use HasFactory;

    use SoftDeletes;
    protected $dates=['deleted_at'];

    protected $table='grn';



    protected $fillable=[
        'grn_number',
        'grn_title',
        'grn_description',
        'grn_created_by',
        'is_approved',
        'grn_approved_by',
        'quentity',
        'supplier',
    ];

}
