<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImage extends Model
{
    use HasFactory;

    use SoftDeletes;
    protected $dates=['deleted_at'];

    protected $table='product_images';

    protected $fillable=['name','title','order','image','product_id'];
}
