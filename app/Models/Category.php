<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;

    protected $table='category';

    use SoftDeletes;
    protected $dates=['deleted_at'];




    protected $fillable = ['name','description','image'];

    public function products_cat()
    {
        return $this->belongsToMany(Product::class,Product_cat::class)->withTimeStamps();
    }


    public function subcategories()
    {
        return $this->hasMany(SubCategory::class,'parent_id','id');
    }

    public function products()
    {
        return $this->hasMany(Product::class,'product_id','id');

    }
    public function subsubCategories()
    {
        return $this->hasMany(Product::class,'sub_subcategory_id','id');
    }










}
