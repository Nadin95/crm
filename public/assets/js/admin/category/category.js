var category={

    deletecategory:function(id){

        var confirmation = confirm("Are you sure ?");

        if(confirmation){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/category/deletecategory",
                type: "POST",
                cache: false,
                data: {
                    id:id
                },
                success: function (response) {

                    if(response=='success')
                    {
                        alert('Deleted Successfully');
                        window.history.back()
                    }






                }, error: function (xhr) {
                    console.log(alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText));
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                }
            })
        }

    },
    delete_subcategory:function(id)
    {

        var confirmation = confirm("Are you sure ?");

        if(confirmation){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/category/subcategory/delete",
                type: "POST",
                cache: false,
                data: {
                    id:id
                },
                success: function (response) {

                    if(response=='success')
                    {
                        alert('Deleted Successfully');
                        window.history.back()
                    }






                }, error: function (xhr) {
                    console.log(alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText));
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                }
            })
        }

    },

    delete_subsub_category:function(id)
    {

        var confirmation = confirm("Are you sure ?");

        if(confirmation){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/category/subcategory/subsubcategory/delete",
                type: "POST",
                cache: false,
                data: {
                    id:id
                },
                success: function (response) {

                    if(response=='success')
                    {
                        alert('Deleted Successfully');
                        window.history.back()
                    }






                }, error: function (xhr) {
                    console.log(alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText));
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                }
            })
        }

    }
}
