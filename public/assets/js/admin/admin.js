var admin={

    create: function () {


    var name=$('#name').val();
    var email=$('#email').val();
    var contact_number=$('#contact').val();
    var password=$('#password').val();
    var re_pass=$('#re_pass').val();
    var h_id=$('#edit_id').val();

    if(password != re_pass)
    {
        alert ('Password is not Match');
        return false;
    }



        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':
                    $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "/admin/CreateAdmin",
            type: "POST",
            cache: false,
            data: {

                name:name,
                email:email,
                contact_number:contact_number,
                password:password,
                h_id:h_id


            },
            success: function (response) {
                if(response=='success'){
                    alert('Success');
                    location.reload();
                }
                if(response=='Updated'){
                    alert('Updated');
                    location.reload();
                }




            }, error: function (xhr) {
                console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);

                $('#nameError').text(xhr.responseJSON.errors.name);
                $('#emailError').text(xhr.responseJSON.errors.email);
                $('#contactError').text(xhr.responseJSON.errors.contact_number);
                $('#passwordError').text(xhr.responseJSON.errors.password);








            }
        })


    },
    delete:function(id){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':
                    $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "/admin/DeleteAdmin/"+id,
            type: "POST",
            cache: false,
            data: {
                id:id
            },
            success: function (response) {
                if(response=='success'){
                    alert('Deleted');
                    location.reload();
                }


            }, error: function (xhr) {
                console.log(alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText));
                alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
            }
        })

    },

    edit:function(id){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':
                    $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "/admin/getEditAdmin",
            type: "POST",
            cache: false,
            data: {
                id:id
            },
            success: function (response) {

                $('#edit_id').val(response[0].id);
                $('#name').val(response[0].name);
                $('#email').val(response[0].email);
                $('#contact').val(response[0].contact_number);
                $('')


            }, error: function (xhr) {
                console.log(alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText));
                alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
            }
        })

    }
}
