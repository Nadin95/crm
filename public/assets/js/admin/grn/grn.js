var grn={
    create: function () {


        var grn_title=$('#grn_title').val();
        var grn_description=$('#grn_description').val();
        var quentity=$('#quentity').val();
        var supplier=$('#supplier').val();
        var approved=$('#approved_status:checked').val();

        if(approved !=1 ){
            approved=0;
        }


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/grn/creategrn",
                type: "POST",
                cache: false,
                data: {

                    grn_title:grn_title,
                    grn_description:grn_description,
                    quentity:quentity,
                    supplier:supplier,
                    approved:approved,



                },
                success: function (response) {

                    location.href ='edit/'+response;



                }, error: function (xhr) {
                    console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);

                    $('#title_error').text(xhr.responseJSON.errors.grn_title);
                    $('#description_error').text(xhr.responseJSON.errors.grn_description );
                    $('#quertity_error').text(xhr.responseJSON.errors.quentity );
                    $('#supplier_error').text(xhr.responseJSON.errors.supplier);



                }
            })


    },
    variantadd:function()
    {
        // alert('hi');

        var code=$('#grn_code').val();
        var variant=$('#variant').val();
        var quantity=$('#quantity').val();
        var unit_price=$('#unit_price').val();
        var processed=$('#processed:checked').val();

        if(processed !=1 ){
            processed=0;
        }



        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':
                    $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "/admin/grn/update",
            type: "POST",
            cache: false,
            data: {

                code:code,
                variant:variant,
                quantity:quantity,
                unit_price:unit_price,
                processed:processed,



            },
            success: function (response) {

                if(response=='success')
                {

                    $(".alert-success").css("display", "block");
                    $(".alert-success").append("<strong>sucessfully updated</strong>");

                }



            }, error: function (xhr) {
                console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);

                $('#grn_code_error').text(xhr.responseJSON.errors.grn_id);
                $('#variant_error').text(xhr.responseJSON.errors.product_variant);
                $('#quantity_error').text(xhr.responseJSON.errors.quentity);
                $('#unit_price_error').text(xhr.responseJSON.errors.unite_price );







            }
        })



    },
    viewEditgrn:function(id)
    {


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':
                    $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "/admin/grn/updategrn/"+id,
            type: "GET",
            cache: false,
            data: {



            },
            success: function (response) {




            }, error: function (xhr) {
                console.log(alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText));









            }
        })
    },
    update:function()
    {
        alert('hi');

        var grn_title=$('#grn_title').val();
        var grn_description=$('#grn_description').val();
        var quentity=$('#quentity').val();
        var supplier=$('#supplier').val();
        var approved=$('#approved_status:checked').val();

        if(approved !=1 ){
            approved=0;
        }


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/grn/update",
                type: "POST",
                cache: false,
                data: {

                    grn_title:grn_title,
                    grn_description:grn_description,
                    quentity:quentity,
                    supplier:supplier,
                    approved:approved,



                },
                success: function (response) {

                    location.href ='edit/'+response;



                }, error: function (xhr) {
                    console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);

                    $('#title_error').text(xhr.responseJSON.errors.grn_title);
                    $('#description_error').text(xhr.responseJSON.errors.grn_description );
                    $('#quertity_error').text(xhr.responseJSON.errors.quentity );
                    $('#supplier_error').text(xhr.responseJSON.errors.supplier);



                }
            })

    }



}
